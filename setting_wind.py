#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
from PyQt5.QtWidgets import (QWidget, QLabel, QPushButton, QApplication,
                             QLineEdit, QApplication, QMainWindow, QAction, qApp, QComboBox, QDialog)
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap, QColor, QIcon, QFont
from PyQt5.QtCore import QCoreApplication, QTimer, QSize, QRect
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal

import PyQt5.QtCore as QtCore


from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient

import requests
import configparser

import serial

type_abbreviature = 'complex_1'

# --->>> на дисплей
# <<<--- на контроллер

def get_config(path):
    """
    Returns the config object
    """
    if not os.path.exists(path):
        # create_config(path)
        config = configparser.ConfigParser()
        with open(path, "w") as config_file:
            config.write(config_file)
    
    config = configparser.ConfigParser()
    config.read(path)
    return config


def serial_ports():
    """ Lists serial port names
        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


class extQLineEdit(QLineEdit):
    clicked = pyqtSignal()

    def __init__(self, parent):
        QLineEdit.__init__(self, parent)

    def mousePressEvent(self, QMouseEvent):
        self.clicked.emit()


class NumPad(QDialog):
    def __init__(self):
        super().__init__()
        self.setupUi()

        self.value = ""

    def setupUi(self):
        self.btn_1 = QPushButton(self)
        self.btn_1.setText("1")
        self.btn_1.move(0, 0)
        self.btn_1.resize(75, 75)
        self.btn_1.clicked.connect(self.btn_1_click)

        self.btn_2 = QPushButton(self)
        self.btn_2.setText("2")
        self.btn_2.move(75, 0)
        self.btn_2.resize(75, 75)
        self.btn_2.clicked.connect(self.btn_2_click)

        self.btn_3 = QPushButton(self)
        self.btn_3.setText("3")
        self.btn_3.move(150, 0)
        self.btn_3.resize(75, 75)
        self.btn_3.clicked.connect(self.btn_3_click)

        self.btn_4 = QPushButton(self)
        self.btn_4.setText("4")
        self.btn_4.move(0, 75)
        self.btn_4.resize(75, 75)
        self.btn_4.clicked.connect(self.btn_4_click)

        self.btn_5 = QPushButton(self)
        self.btn_5.setText("5")
        self.btn_5.move(75, 75)
        self.btn_5.resize(75, 75)
        self.btn_5.clicked.connect(self.btn_5_click)

        self.btn_6 = QPushButton(self)
        self.btn_6.setText("6")
        self.btn_6.move(150, 75)
        self.btn_6.resize(75, 75)
        self.btn_6.clicked.connect(self.btn_6_click)

        self.btn_7 = QPushButton(self)
        self.btn_7.setText("7")
        self.btn_7.move(0, 150)
        self.btn_7.resize(75, 75)
        self.btn_7.clicked.connect(self.btn_7_click)

        self.btn_8 = QPushButton(self)
        self.btn_8.setText("8")
        self.btn_8.move(75, 150)
        self.btn_8.resize(75, 75)
        self.btn_8.clicked.connect(self.btn_8_click)

        self.btn_9 = QPushButton(self)
        self.btn_9.setText("9")
        self.btn_9.move(150, 150)
        self.btn_9.resize(75, 75)
        self.btn_9.clicked.connect(self.btn_9_click)

        self.btn_0 = QPushButton(self)
        self.btn_0.setText("0")
        self.btn_0.move(75, 225)
        self.btn_0.resize(75, 75)
        self.btn_0.clicked.connect(self.btn_0_click)

        self.btn_point = QPushButton(self)
        self.btn_point.setText(".")
        self.btn_point.move(0, 225)
        self.btn_point.resize(75, 75)
        self.btn_point.clicked.connect(self.btn_point_click)

        self.btn_backspace = QPushButton(self)
        self.btn_backspace.setText("<-")
        self.btn_backspace.move(150, 225)
        self.btn_backspace.resize(75, 75)
        self.btn_backspace.clicked.connect(self.btn_backspace_click)

        self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        # self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)
        # self.setWindowFlags(Qt.Dialog | QtCore.Qt.FramelessWindowHint)
        self.setGeometry(100, 100, 225, 300)
        self.setWindowTitle("NumPad")

    def btn_1_click(self):
        self.value = self.value + "1"
        print(self.value)

    def btn_2_click(self):
        self.value = self.value + "2"
        print(self.value)

    def btn_3_click(self):
        self.value = self.value + "3"
        print(self.value)

    def btn_4_click(self):
        self.value = self.value + "4"
        print(self.value)

    def btn_5_click(self):
        self.value = self.value + "5"
        print(self.value)

    def btn_6_click(self):
        self.value = self.value + "6"
        print(self.value)

    def btn_7_click(self):
        self.value = self.value + "7"
        print(self.value)

    def btn_8_click(self):
        self.value = self.value + "8"
        print(self.value)

    def btn_9_click(self):
        self.value = self.value + "9"
        print(self.value)

    def btn_0_click(self):
        self.value = self.value + "0"
        print(self.value)

    def btn_point_click(self):
        self.value = self.value + "."
        print(self.value)

    def btn_backspace_click(self):
        self.value = self.value[0:-1]
        print(self.value)


class Window_com_port(QMainWindow):
    def __init__(self, root):
        super().__init__(root)
        self.setupUi()
        # self.Port.addItems(serial_ports())
        self.realport = None
        # self.ConnectButton.clicked.connect(self.connect_modbus)

        self.connect_status = 0
        self.connect_status_flag = 1
        self.flag_after_first_connect = 0
        self.flag_war_msg = 0
        # self.client = 0
        self.client = ModbusSerialClient()
        self.port = ""

    def setupUi(self):

        self.Port = QComboBox(self)
        self.Port.setGeometry(QRect(10, 30, 381, 31))
        self.Port.setObjectName("Port")

        self.label = QLabel(self)
        self.label.move(170, 0)
        font = QFont()
        font.setPointSize(8)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label.setText("Порт")

        self.ConnectButton = QPushButton(self)
        self.ConnectButton.setText("Подключиться")
        self.ConnectButton.move(10, 70)
        self.ConnectButton.clicked.connect(self.connect_modbus)

        # self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 400, 300)
        self.setWindowTitle("Настройка СOM порта")

    def set_ports(self):
        self.Port.clear()
        self.Port.addItems(serial_ports())
        print(serial_ports())

        print("+"*60)

    def connect_modbus(self):
        try:
            # self.port = self.Port.currentText()
            self.client = ModbusSerialClient(method="rtu", port=self.Port.currentText(
            ), stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
            print(self.client)
            # connection = self.client.connect()
            # self.ConnectButton.setStyleSheet("background-color: green")

            self.ConnectButton.setText('Подключено')
            self.ConnectButton.setEnabled(False)

            # добавить отдельноеполе на параметр подключения
            # self.connect_status = self.client.connect()
            self.flag_after_first_connect = 1
            self.flag_war_msg = 1
            print("Connect_status", self.connect_status)

        except Exception as e:
            print(e)


class Window_web(QMainWindow):
    def __init__(self, root):
        super().__init__(root)
        self.setupUi()
        # self.Port.addItems(serial_ports())



    def setupUi(self):

        self.qle_to_1 = extQLineEdit(self)
        self.qle_to_1.resize(200, 25)
        self.qle_to_1.move(10, 30)


        self.label = QLabel(self)
        self.label.move(10, 0)
        self.label.resize(200,25)
        font = QFont()
        font.setPointSize(8)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label.setText("Укажите название таблицы")

        self.set_table_Button = QPushButton(self)
        self.set_table_Button.setText("Указать таблицу")
        self.set_table_Button.resize(150,25)
        self.set_table_Button.move(10, 70)
        self.set_table_Button.clicked.connect(self.set_table)

        # self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 300, 200)
        self.setWindowTitle("Настройки для отправки данных на сервер")

    def set_table(self):
        print("x")
        self.unique_name = self.qle_to_1.text()
        url = 'http://188.225.39.107/CRUSHER2/aggregate.php'
        response = requests.post(url, data={"action": "set_name_to_scada", "unique_name" : self.unique_name, "type_abbreviature": type_abbreviature})


        print(response)
        print(response.text)


        if(response.text == "no_name"):

            msg = QMessageBox()
            msg.setWindowTitle("Warning")
            msg.setText("Агрегата с таким именем не существует")
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()

        elif(response.text == "type_error"):
            # тут проверка на соответствие 

            msg = QMessageBox()
            msg.setWindowTitle("Warning")
            msg.setText("Тип указаного агрегата не соответствует SCADA")
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()
        
        elif(response.text == "ok"): 

            self.createConfig()

            msg = QMessageBox()
            msg.setWindowTitle("Massage")
            msg.setText("Имя таблицы задано. Тип указаного агрегата соответствует SCADA.")
            msg.setIcon(QMessageBox.Information)
            msg.exec_()
        
        

    def createConfig(self):
        """
        Create a config file
        """
        path = "settings.ini"
       
        # config = configparser.ConfigParser()
        config  = get_config(path)
 
        try:
            # Window_setting_hammer
            config.set("Settings", "unique_name", str(self.unique_name))

        except:
            config.add_section("Settings")
            # Window_setting_hammer
            config.set("Settings", "unique_name", str(self.unique_name))
        

        with open(path, "w") as config_file:
            config.write(config_file)






class Window_setting(QDialog):                           # <===
    def __init__(self, root):
        super().__init__(root)

        # Настройки
        # <<<---
        # время вкл выкидного транспортера
        self.time_on_conveyor_1 = 0
        # время выкл выкидного транспортера
        self.time_off_conveyor_1 = 0
        # время вкл подающего транспортера
        self.time_on_conveyor_2 = 0
        # время выкл подающего транспортера
        self.time_off_conveyor_2 = 0

        # время выключения молотковой дробилки
        self.time_off_hammer = 0
        # предпусковой звонок
        self.pre_launch = 0
        # время прямого вращения
        self.time_direct_rotation = 0
        # время обратного вражения
        self.time_reverse_rotation = 0
        # пауза на остановку
        self.pause_to_stop = 0

        self.time_on_conveyor_1_status = 0
        self.time_off_conveyor_1_status = 0
        self.time_on_conveyor_2_status = 0
        self.time_off_conveyor_2_status = 0

        self.time_off_hammer_status = 0
        self.pre_launch_status = 0
        self.time_direct_rotation_status = 0
        self.pause_to_stop_status = 0

        self.status_but = 0
        self.y = 0

        self.param = 0
        self.numpad = NumPad()

        self.numpad.btn_1.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_2.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_3.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_4.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_5.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_6.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_7.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_8.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_9.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_0.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_point.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_backspace.clicked.connect(self.put_data_to_edit)

        # таймер на обработку
        # self.timer = QTimer(self)
        # self.timer.timeout.connect(self.put_data_to_edit)
        # self.timer.start(100)

        self.qbtn_load = QPushButton('Установить настройки', self)
        self.qbtn_load.clicked.connect(self.load_setting_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_load.adjustSize()
        self.qbtn_load.move(50, 500)

        self.qbtn_set_standart = QPushButton('Задать стандартные настройки', self)
        self.qbtn_set_standart.clicked.connect(self.set_standart_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_set_standart.adjustSize()
        self.qbtn_set_standart.move(50, 550)

        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.show_NumPad)
        self.qbtn_2_m.setIcon(QIcon('img/kay_bord.png'))
        self.qbtn_2_m.setToolTip('Включить клавиатуру')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(500, 500)

        self.lbl_to = QLabel(self)
        self.lbl_to.setText("Задать настройки на ПЛК")
        self.lbl_to.adjustSize()
        self.lbl_to.move(50, 10)

        self.y = self.y + 50
        self.lbl_to_1 = QLabel(self)
        self.lbl_to_1.setText("Время вкл. выкидного транспортера")
        self.lbl_to_1.adjustSize()
        self.lbl_to_1.move(50, self.y)

        self.qle_to_1 = extQLineEdit(self)
        self.qle_to_1.resize(200, 25)
        self.qle_to_1.move(50, self.y + 20)
        self.qle_to_1.textChanged[str].connect(self.onChanged_1)
        self.qle_to_1.clicked.connect(lambda i="s_l1": self.set_param(i))

        # self.lbl_to_2 = QLabel(self)
        # self.lbl_to_2.setText("Время выкл. выкидного транспортера")
        # self.lbl_to_2.adjustSize()
        # self.lbl_to_2.move(50, 100)

        # self.qle_to_2 = extQLineEdit(self)
        # self.qle_to_2.resize(200, 25)
        # self.qle_to_2.move(50, 120)
        # self.qle_to_2.textChanged[str].connect(self.onChanged_2)
        # self.qle_to_2.clicked.connect(lambda i="s_l2": self.set_param(i))
        # self.qle_to_2.setReadOnly(1)
        # self.qle_to_2.setStyleSheet('background : #c4c4c4; ')

        self.y = self.y + 50
        self.lbl_to_3 = QLabel(self)
        self.lbl_to_3.setText("Время вкл. подающего транспортера")
        self.lbl_to_3.adjustSize()
        self.lbl_to_3.move(50, self.y)

        self.qle_to_3 = extQLineEdit(self)
        self.qle_to_3.resize(200, 25)
        self.qle_to_3.move(50, self.y + 20)
        self.qle_to_3.textChanged[str].connect(self.onChanged_3)
        self.qle_to_3.clicked.connect(lambda i="s_l3": self.set_param(i))

        self.y = self.y + 50
        self.lbl_to_4 = QLabel(self)
        self.lbl_to_4.setText("Время выкл. подающего транспортера")
        self.lbl_to_4.adjustSize()
        self.lbl_to_4.move(50, self.y)

        self.qle_to_4 = extQLineEdit(self)
        self.qle_to_4.resize(200, 25)
        self.qle_to_4.move(50, self.y + 20)
        self.qle_to_4.textChanged[str].connect(self.onChanged_4)
        self.qle_to_4.clicked.connect(lambda i="s_l4": self.set_param(i))

        self.y = self.y + 50
        self.lbl_to_5 = QLabel(self)
        self.lbl_to_5.setText("Время выкл. молотковой дробилки")
        self.lbl_to_5.adjustSize()
        self.lbl_to_5.move(50, self.y)

        self.qle_to_5 = extQLineEdit(self)
        self.qle_to_5.resize(200, 25)
        self.qle_to_5.move(50, self.y + 20)
        self.qle_to_5.textChanged[str].connect(self.onChanged_5)
        self.qle_to_5.clicked.connect(lambda i="s_l5": self.set_param(i))

        # self.y = self.y + 50
        # self.lbl_to_6 = QLabel(self)
        # self.lbl_to_6.setText("Предпусковой звонок (вкл/выкл)")
        # self.lbl_to_6.adjustSize()
        # self.lbl_to_6.move(50, 300)

        # self.qle_to_6 = extQLineEdit(self)
        # self.qle_to_6.resize(200, 25)
        # self.qle_to_6.move(50, 320)
        # self.qle_to_6.textChanged[str].connect(self.onChanged_6)
        # self.qle_to_6.clicked.connect(lambda i="s_l6": self.set_param(i))

        self.y = self.y + 50
        self.lbl_to_7 = QLabel(self)
        self.lbl_to_7.setText("Время прямого вращения")
        self.lbl_to_7.adjustSize()
        self.lbl_to_7.move(50, self.y)

        self.qle_to_7 = extQLineEdit(self)
        self.qle_to_7.resize(200, 25)
        self.qle_to_7.move(50, self.y + 20)
        self.qle_to_7.textChanged[str].connect(self.onChanged_7)
        self.qle_to_7.clicked.connect(lambda i="s_l7": self.set_param(i))

        self.y = self.y + 50
        self.lbl_to_8 = QLabel(self)
        self.lbl_to_8.setText("Время обратного вращения")
        self.lbl_to_8.adjustSize()
        self.lbl_to_8.move(50, self.y)

        self.qle_to_8 = extQLineEdit(self)
        self.qle_to_8.resize(200, 25)
        self.qle_to_8.move(50, self.y + 20)
        self.qle_to_8.textChanged[str].connect(self.onChanged_8)
        self.qle_to_8.clicked.connect(lambda i="s_l8": self.set_param(i))

        self.y = self.y + 50
        self.lbl_to_9 = QLabel(self)
        self.lbl_to_9.setText("Пауза на остановку")
        self.lbl_to_9.adjustSize()
        self.lbl_to_9.move(50, self.y)

        self.qle_to_9 = extQLineEdit(self)
        self.qle_to_9.resize(200, 25)
        self.qle_to_9.move(50, self.y + 20)
        self.qle_to_9.textChanged[str].connect(self.onChanged_9)
        self.qle_to_9.clicked.connect(lambda i="s_l9": self.set_param(i))

        self.y = 0

        
        self.lbl_from = QLabel(self)
        self.lbl_from.setText("Текущие настройки на ПЛК")
        self.lbl_from.adjustSize()
        self.lbl_from.move(350, 10)

        self.y = self.y + 50
        self.lbl_from_1 = QLabel(self)
        self.lbl_from_1.setText("Время вкл. выкидного транспортера")
        self.lbl_from_1.adjustSize()
        self.lbl_from_1.move(350, self.y)

        self.qle_from_1 = QLineEdit(self)
        self.qle_from_1.resize(200, 25)
        self.qle_from_1.move(350, self.y + 20)
        self.qle_from_1.setReadOnly(1)

        # self.y = self.y + 50
        # self.lbl_from_2 = QLabel(self)
        # self.lbl_from_2.setText("Время выкл. выкидного транспортера")
        # self.lbl_from_2.adjustSize()
        # self.lbl_from_2.move(350, 100)

        # self.qle_from_2 = QLineEdit(self)
        # self.qle_from_2.resize(200, 25)
        # self.qle_from_2.move(350, 120)
        # self.qle_from_2.setReadOnly(1)

        self.y = self.y + 50
        self.lbl_from_3 = QLabel(self)
        self.lbl_from_3.setText("Время вкл. подающего транспортера")
        self.lbl_from_3.adjustSize()
        self.lbl_from_3.move(350, self.y)

        self.qle_from_3 = QLineEdit(self)
        self.qle_from_3.resize(200, 25)
        self.qle_from_3.move(350, self.y + 20)
        self.qle_from_3.setReadOnly(1)

        self.y = self.y + 50
        self.lbl_from_4 = QLabel(self)
        self.lbl_from_4.setText("Время выкл. подающего транспортера")
        self.lbl_from_4.adjustSize()
        self.lbl_from_4.move(350, self.y)

        self.qle_from_4 = QLineEdit(self)
        self.qle_from_4.resize(200, 25)
        self.qle_from_4.move(350, self.y + 20)
        self.qle_from_4.setReadOnly(1)

        self.y = self.y + 50
        self.lbl_from_5 = QLabel(self)
        self.lbl_from_5.setText("Время выкл. молотковой дробилки")
        self.lbl_from_5.adjustSize()
        self.lbl_from_5.move(350, self.y)

        self.qle_from_5 = QLineEdit(self)
        self.qle_from_5.resize(200, 25)
        self.qle_from_5.move(350, self.y + 20)
        self.qle_from_5.setReadOnly(1)

        # self.y = self.y + 50
        # self.lbl_from_6 = QLabel(self)
        # self.lbl_from_6.setText("Предпусковой звонок (вкл/выкл)")
        # self.lbl_from_6.adjustSize()
        # self.lbl_from_6.move(350, 300)

        # self.qle_from_6 = QLineEdit(self)
        # self.qle_from_6.resize(200, 25)
        # self.qle_from_6.move(350, 320)
        # self.qle_from_6.setReadOnly(1)

        self.y = self.y + 50
        self.lbl_from_7 = QLabel(self)
        self.lbl_from_7.setText("Время прямого вращения")
        self.lbl_from_7.adjustSize()
        self.lbl_from_7.move(350, self.y)

        self.qle_from_7 = QLineEdit(self)
        self.qle_from_7.resize(200, 25)
        self.qle_from_7.move(350, self.y + 20)
        self.qle_from_7.setReadOnly(1)

        self.y = self.y + 50
        self.lbl_from_8 = QLabel(self)
        self.lbl_from_8.setText("Время обратного вращения")
        self.lbl_from_8.adjustSize()
        self.lbl_from_8.move(350, self.y)

        self.qle_from_8 = QLineEdit(self)
        self.qle_from_8.resize(200, 25)
        self.qle_from_8.move(350, self.y + 20)
        self.qle_from_8.setReadOnly(1)

        self.y = self.y + 50
        self.lbl_from_9 = QLabel(self)
        self.lbl_from_9.setText("Пауза на остановку")
        self.lbl_from_9.adjustSize()
        self.lbl_from_9.move(350, self.y)

        self.qle_from_9 = QLineEdit(self)
        self.qle_from_9.resize(200, 25)
        self.qle_from_9.move(350, self.y +20)
        self.qle_from_9.setReadOnly(1)

        # self.setWindowFlag(QtCore.Qt.Dialog)
        # self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 600, 600)
        self.setWindowTitle("Настройки")

    def createConfig(self):
        """
        Create a config file
        """
        path = "settings.ini"
       
        # config = configparser.ConfigParser()
        config  = get_config(path)
 
        try:
            # Window_setting
            config.set("Settings", "time_on_conveyor_1", str(self.time_on_conveyor_1))
            config.set("Settings", "time_on_conveyor_2", str(self.time_on_conveyor_2))
            config.set("Settings", "time_off_conveyor_2", str(self.time_off_conveyor_2))
            config.set("Settings", "time_off_hammer", str(self.time_off_hammer))
            config.set("Settings", "time_direct_rotation", str(self.time_direct_rotation))
            config.set("Settings", "time_reverse_rotation", str(self.time_reverse_rotation))
            config.set("Settings", "pause_to_stop", str(self.pause_to_stop))
        except:
            config.add_section("Settings")
            # Window_setting
            config.set("Settings", "time_on_conveyor_1", str(self.time_on_conveyor_1))
            config.set("Settings", "time_on_conveyor_2", str(self.time_on_conveyor_2))
            config.set("Settings", "time_off_conveyor_2", str(self.time_off_conveyor_2))
            config.set("Settings", "time_off_hammer", str(self.time_off_hammer))
            config.set("Settings", "time_direct_rotation", str(self.time_direct_rotation))
            config.set("Settings", "time_reverse_rotation", str(self.time_reverse_rotation))
            config.set("Settings", "pause_to_stop", str(self.pause_to_stop))
        

        with open(path, "w") as config_file:
            config.write(config_file)

    def set_standart_data(self):

        self.time_on_conveyor_1 = 3
        # self.time_off_conveyor_1 = 0
        self.time_on_conveyor_2 = 3
        self.time_off_conveyor_2 = 3
        self.time_off_hammer = 40
        # self.pre_launch = 0
        self.time_direct_rotation = 20
        self.time_reverse_rotation = 4
        self.pause_to_stop = 3
        
        
        self.qle_to_1.setText(str(self.time_on_conveyor_1))
        # self.qle_to_2.setText(str(self.))
        self.qle_to_3.setText(str(self.time_on_conveyor_2))
        self.qle_to_4.setText(str(self.time_off_conveyor_2))
        self.qle_to_5.setText(str(self.time_off_hammer))
        # self.qle_to_6.setText(str(self.))
        self.qle_to_7.setText(str(self.time_direct_rotation))
        self.qle_to_8.setText(str(self.time_reverse_rotation))
        self.qle_to_9.setText(str(self.pause_to_stop))

    def put_data_to_edit(self):
        if self.param == 's_l1':
            self.qle_to_1.setText(self.numpad.value)
        if self.param == 's_l2':
            self.qle_to_2.setText(self.numpad.value)
        if self.param == 's_l3':
            self.qle_to_3.setText(self.numpad.value)
        if self.param == 's_l4':
            self.qle_to_4.setText(self.numpad.value)
        if self.param == 's_l5':
            self.qle_to_5.setText(self.numpad.value)
        if self.param == 's_l6':
            self.qle_to_6.setText(self.numpad.value)
        if self.param == 's_l7':
            self.qle_to_7.setText(self.numpad.value)
        if self.param == 's_l8':
            self.qle_to_8.setText(self.numpad.value)
        if self.param == 's_l9':
            self.qle_to_9.setText(self.numpad.value)

    def set_param(self, param):
        self.param = param
        print(self.param)
        if self.param == 's_l1':
            self.numpad.value = self.qle_to_1.text()
        if self.param == 's_l2':
            self.numpad.value = self.qle_to_2.text()
        if self.param == 's_l3':
            self.numpad.value = self.qle_to_3.text()
        if self.param == 's_l4':
            self.numpad.value = self.qle_to_4.text()
        if self.param == 's_l5':
            self.numpad.value = self.qle_to_5.text()
        if self.param == 's_l6':
            self.numpad.value = self.qle_to_6.text()
        if self.param == 's_l7':
            self.numpad.value = self.qle_to_7.text()
        if self.param == 's_l8':
            self.numpad.value = self.qle_to_8.text()
        if self.param == 's_l9':
            self.numpad.value = self.qle_to_9.text()

    def show_NumPad(self):
        self.numpad.move(700, 500)
        self.numpad.show()

    def load_setting_data(self):
        print("load_satting")
        self.status_but = 1
        self.createConfig()

    def load_kay_bord(self):
        os.startfile(r'C:\Windows\System32\osk.exe')

    def onChanged_1(self, text):
        try:
            self.time_on_conveyor_1 = float(text)
            self.qle_to_1.setStyleSheet('background : #ffffff; ')
            self.time_on_conveyor_1_status = 1
        except ValueError:
            self.qle_to_1.setStyleSheet('background : #ff4000; ')
            self.time_on_conveyor_1_status = 0
            print("ValueError")

    def onChanged_2(self, text):
        try:
            self.time_off_conveyor_1 = float(text)
            self.qle_to_2.setStyleSheet('background : #ffffff; ')
            self.time_off_conveyor_1_status = 1
        except ValueError:
            self.qle_to_2.setStyleSheet('background : #ff4000; ')
            self.time_off_conveyor_1_status = 0
            print("ValueError")

    def onChanged_3(self, text):
        try:
            self.time_on_conveyor_2 = float(text)
            self.qle_to_3.setStyleSheet('background : #ffffff; ')
            self.time_on_conveyor_2_status = 1
        except ValueError:
            self.qle_to_3.setStyleSheet('background : #ff4000; ')
            self.time_on_conveyor_2_status = 0
            print("ValueError")

    def onChanged_4(self, text):
        try:
            self.time_off_conveyor_2 = float(text)
            self.qle_to_4.setStyleSheet('background : #ffffff; ')
            self.time_off_conveyor_2_status = 1
        except ValueError:
            self.qle_to_4.setStyleSheet('background : #ff4000; ')
            self.time_off_conveyor_2_status = 0
            print("ValueError")

    def onChanged_5(self, text):
        try:
            self.time_off_hammer = float(text)
            self.qle_to_5.setStyleSheet('background : #ffffff; ')
            self.time_off_hammer_status = 1
        except ValueError:
            self.qle_to_5.setStyleSheet('background : #ff4000; ')
            self.time_off_hammer_status = 0
            print("ValueError")

    def onChanged_6(self, text):
        try:
            self.pre_launch = float(text)
            self.qle_to_6.setStyleSheet('background : #ffffff; ')
            self.pre_launch_status = 1
        except ValueError:
            self.qle_to_6.setStyleSheet('background : #ff4000; ')
            self.pre_launch_status = 0
            print("ValueError")

    def onChanged_7(self, text):
        try:
            self.time_direct_rotation = float(text)
            self.qle_to_7.setStyleSheet('background : #ffffff; ')
            self.time_direct_rotation_status = 1
        except ValueError:
            self.qle_to_7.setStyleSheet('background : #ff4000; ')
            self.time_direct_rotation_status = 0
            print("ValueError")

    def onChanged_8(self, text):
        try:
            self.time_reverse_rotation = float(text)
            self.qle_to_8.setStyleSheet('background : #ffffff; ')
            self.time_reverse_rotation_status = 1
        except ValueError:
            self.qle_to_8.setStyleSheet('background : #ff4000; ')
            self.time_reverse_rotation_status = 0
            print("ValueError")

    def onChanged_9(self, text):
        try:
            self.pause_to_stop = float(text)
            self.qle_to_9.setStyleSheet('background : #ffffff; ')
            self.pause_to_stop_status = 1
        except ValueError:
            self.qle_to_9.setStyleSheet('background : #ff4000; ')
            self.pause_to_stop_status = 0
            print("ValueError")


class Window_setting_hammer(QMainWindow):                           # <===
    def __init__(self, root):
        super().__init__(root)

        # <<<---
        # установка скорости мин (отключение транспортера)
        self.hammer_set_off_speed_min = 0
        # установка скорости мин (включение транспортера)
        self.hammer_set_speed_on = 0
        # установка времени выкл. молотковой авария
        self.time_molot_off_crush = 0

        self.status_but = 0

        self.param = 0
        self.numpad = NumPad()

        self.numpad.btn_1.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_2.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_3.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_4.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_5.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_6.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_7.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_8.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_9.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_0.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_point.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_backspace.clicked.connect(self.put_data_to_edit)

        # таймер на обработку

        # self.timer = QTimer(self)
        # self.timer.timeout.connect(self.put_data_to_edit)
        # self.timer.start(100)

        self.qbtn_load = QPushButton('Установить настройки', self)
        self.qbtn_load.clicked.connect(self.load_setting_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_load.adjustSize()
        self.qbtn_load.move(50, 200)
        
        self.qbtn_set_standart = QPushButton('Задать стандартные настройки', self)
        self.qbtn_set_standart.clicked.connect(self.set_standart_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_set_standart.adjustSize()
        self.qbtn_set_standart.move(50, 250)

        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.show_NumPad)
        self.qbtn_2_m.setIcon(QIcon('img/kay_bord.png'))
        self.qbtn_2_m.setToolTip('Включить клавиатуру')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(600, 200)

        self.lbl_to = QLabel(self)
        self.lbl_to.setText("Задать настройки на ПЛК")
        self.lbl_to.adjustSize()
        self.lbl_to.move(50, 10)

        self.lbl_to_1 = QLabel(self)
        self.lbl_to_1.setText("Уставка скорости min (откл. транспортера)")
        self.lbl_to_1.adjustSize()
        self.lbl_to_1.move(50, 50)

        self.qle_to_1 = extQLineEdit(self)
        self.qle_to_1.resize(200, 25)
        self.qle_to_1.move(50, 70)
        self.qle_to_1.textChanged[str].connect(self.onChanged_1)
        self.qle_to_1.clicked.connect(lambda i="sh_l1": self.set_param(i))

        self.lbl_to_2 = QLabel(self)
        self.lbl_to_2.setText("Уставка скорости (вкл. транспортера)")
        self.lbl_to_2.adjustSize()
        self.lbl_to_2.move(50, 100)

        self.qle_to_2 = extQLineEdit(self)
        self.qle_to_2.resize(200, 25)
        self.qle_to_2.move(50, 120)
        self.qle_to_2.textChanged[str].connect(self.onChanged_2)
        self.qle_to_2.clicked.connect(lambda i="sh_l2": self.set_param(i))

        self.lbl_to_3 = QLabel(self)
        self.lbl_to_3.setText("Уставка времени послеаварийного выкл. молотковой")
        self.lbl_to_3.adjustSize()
        self.lbl_to_3.move(50, 150)

        self.qle_to_3 = extQLineEdit(self)
        self.qle_to_3.resize(200, 25)
        self.qle_to_3.move(50, 170)
        self.qle_to_3.textChanged[str].connect(self.onChanged_3)
        self.qle_to_3.clicked.connect(lambda i="sh_l3": self.set_param(i))


        self.lbl_from = QLabel(self)
        self.lbl_from.setText("Текущие настройки на ПЛК")
        self.lbl_from.adjustSize()
        self.lbl_from.move(400, 10)

        self.lbl_from_1 = QLabel(self)
        self.lbl_from_1.setText("Уставка скорости min (откл. транспортера)")
        self.lbl_from_1.adjustSize()
        self.lbl_from_1.move(400, 50)

        self.qle_from_1 = QLineEdit(self)
        self.qle_from_1.resize(200, 25)
        self.qle_from_1.move(400, 70)
        self.qle_from_1.setReadOnly(1)

        self.lbl_from_2 = QLabel(self)
        self.lbl_from_2.setText("Уставка скорости (вкл. транспортера)")
        self.lbl_from_2.adjustSize()
        self.lbl_from_2.move(400, 100)

        self.qle_from_2 = QLineEdit(self)
        self.qle_from_2.resize(200, 25)
        self.qle_from_2.move(400, 120)
        self.qle_from_2.setReadOnly(1)

        self.lbl_from_3 = QLabel(self)
        self.lbl_from_3.setText("Уставка времени послеаварийного выкл. молотковой")
        self.lbl_from_3.adjustSize()
        self.lbl_from_3.move(400, 150)

        self.qle_from_3 = extQLineEdit(self)
        self.qle_from_3.resize(200, 25)
        self.qle_from_3.move(400, 170)
        self.qle_from_3.setReadOnly(1)

        # self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 700, 300)
        self.setWindowTitle("Настройки молотковой дробилки")

    def createConfig(self):
        """
        Create a config file
        """
        path = "settings.ini"
       
        # config = configparser.ConfigParser()
        config  = get_config(path)
 
        try:
            # Window_setting_hammer
            config.set("Settings", "hammer_set_off_speed_min", str(self.hammer_set_off_speed_min))
            config.set("Settings", "hammer_set_speed_on", str(self.hammer_set_speed_on))
            config.set("Settings", "time_molot_off_crush", str(self.time_molot_off_crush))
        except:
            config.add_section("Settings")
            # Window_setting_hammer
            config.set("Settings", "hammer_set_off_speed_min", str(self.hammer_set_off_speed_min))
            config.set("Settings", "hammer_set_speed_on", str(self.hammer_set_speed_on))
            config.set("Settings", "time_molot_off_crush", str(self.time_molot_off_crush))
        

        with open(path, "w") as config_file:
            config.write(config_file)
    
    def set_standart_data(self):
        self.hammer_set_off_speed_min = 445
        self.hammer_set_speed_on = 640
        self.time_molot_off_crush = 2 
        
        self.qle_to_1.setText(str(self.hammer_set_off_speed_min))
        self.qle_to_2.setText(str(self.hammer_set_speed_on))
        self.qle_to_3.setText(str(self.time_molot_off_crush))

    def put_data_to_edit(self):
        if self.param == 'sh_l1':
            self.qle_to_1.setText(self.numpad.value)
        if self.param == 'sh_l2':
            self.qle_to_2.setText(self.numpad.value)
        if self.param == 'sh_l3':
            self.qle_to_3.setText(self.numpad.value)

    def set_param(self, param):
        self.param = param
        if self.param == 'sh_l1':
            self.numpad.value = self.qle_to_1.text()
        if self.param == 'sh_l2':
            self.numpad.value = self.qle_to_2.text()
        if self.param == 'sh_l3':
            self.numpad.value = self.qle_to_3.text()

    def show_NumPad(self):
        self.numpad.move(700, 500)
        self.numpad.show()

    def load_setting_data(self):
        print("load_satting_hammer")
        self.status_but = 1
        self.createConfig()

    def load_kay_bord(self):
        os.startfile(r'C:\Windows\System32\osk.exe')

    def onChanged_1(self, text):
        try:
            self.hammer_set_off_speed_min = float(text)
            self.qle_to_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_1.setStyleSheet('background : #ff4000; ')
            print("ValueError___hammer_set_off_speed_min")

    def onChanged_2(self, text):
        try:
            self.hammer_set_speed_on = float(text)
            self.qle_to_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3(self, text):
        try:
            self.time_molot_off_crush = float(text)
            self.qle_to_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")


class Window_tooth_setting(QMainWindow):                           # <===
    def __init__(self, root):
        super().__init__(root)

        # <<<---
        # установка отключения по скорости min
        self.val_set_off_speed_min_1 = 0
        # self.val_set_off_speed_min_2 = 0
        # self.val_set_off_speed_min_3 = 0
        # self.val_set_off_speed_min_4 = 0
        # self.val_set_off_speed_min_5 = 0
        # self.val_set_off_speed_min_6 = 0

        # установка времени включения
        self.val_set_on_time_1 = 0
        # self.val_set_on_time_2 = 0
        # self.val_set_on_time_3 = 0
        # self.val_set_on_time_4 = 0
        # self.val_set_on_time_5 = 0
        # self.val_set_on_time_6 = 0

        # установка времени фильтрации(срабатывание)
        self.val_set_time_actuation_1 = 0
        # self.val_set_time_actuation_2 = 0
        # self.val_set_time_actuation_3 = 0
        # self.val_set_time_actuation_4 = 0
        # self.val_set_time_actuation_5 = 0
        # self.val_set_time_actuation_6 = 0

        # установка времени на отсутствие импульсов
        self.val_set_time_pulse_off_1 = 0
        # self.val_set_time_pulse_off_2 = 0
        # self.val_set_time_pulse_off_3 = 0
        # self.val_set_time_pulse_off_4 = 0
        # self.val_set_time_pulse_off_5 = 0
        # self.val_set_time_pulse_off_6 = 0


        self.status_but = 0

        self.param = 0
        # self.value = ""
        self.numpad = NumPad()
        print(self.numpad.value)

        self.numpad.btn_1.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_2.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_3.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_4.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_5.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_6.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_7.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_8.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_9.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_0.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_point.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_backspace.clicked.connect(self.put_data_to_edit)

        self.qbtn_load = QPushButton('Установить настройки', self)
        self.qbtn_load.clicked.connect(self.load_setting_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_load.adjustSize()
        self.qbtn_load.move(300, 230)

        self.qbtn_set_standart = QPushButton('Задать стандартные настройки', self)
        self.qbtn_set_standart.clicked.connect(self.set_standart_data)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_set_standart.adjustSize()
        self.qbtn_set_standart.move(50, 250)




        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.show_NumPad)
        self.qbtn_2_m.setIcon(QIcon('img/kay_bord.png'))
        self.qbtn_2_m.setToolTip('Включить клавиатуру')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(480, 500)

        self.lbl_to = QLabel(self)
        self.lbl_to.setText("Задать настройки на ПЛК")
        self.lbl_to.adjustSize()
        self.lbl_to.move(50, 50)

        self.lbl_to_1 = QLabel(self)
        self.lbl_to_1.setText("Установка по скорости min")
        self.lbl_to_1.adjustSize()
        self.lbl_to_1.move(0, 100)

        self.lbl_to_2 = QLabel(self)
        self.lbl_to_2.setText("Установка времени включения")
        self.lbl_to_2.adjustSize()
        self.lbl_to_2.move(0, 130)

        self.lbl_to_3 = QLabel(self)
        self.lbl_to_3.setText("Установка времени фильтрации (срабатывания)")
        self.lbl_to_3.adjustSize()
        self.lbl_to_3.move(0, 160)

        self.lbl_to_4 = QLabel(self)
        self.lbl_to_4.setText("Установка времени на отсутствие импульсов")
        self.lbl_to_4.adjustSize()
        self.lbl_to_4.move(0, 190)

        self.lbl_to_m_1 = QLabel(self)
        self.lbl_to_m_1.setText("Вал №1")
        self.lbl_to_m_1.adjustSize()
        self.lbl_to_m_1.move(300, 80)

        self.qle_to_m_1_p_1 = extQLineEdit(self)
        self.qle_to_m_1_p_1.resize(100, 25)
        self.qle_to_m_1_p_1.move(300, 100)
        self.qle_to_m_1_p_1.textChanged[str].connect(self.onChanged_1_p_1)
        self.qle_to_m_1_p_1.clicked.connect(
            lambda i="st_l1_1": self.set_param(i))

        self.qle_to_m_1_p_2 = extQLineEdit(self)
        self.qle_to_m_1_p_2.resize(100, 25)
        self.qle_to_m_1_p_2.move(300, 130)
        self.qle_to_m_1_p_2.textChanged[str].connect(self.onChanged_1_p_2)
        self.qle_to_m_1_p_2.clicked.connect(
            lambda i="st_l1_2": self.set_param(i))

        self.qle_to_m_1_p_3 = extQLineEdit(self)
        self.qle_to_m_1_p_3.resize(100, 25)
        self.qle_to_m_1_p_3.move(300, 160)
        self.qle_to_m_1_p_3.textChanged[str].connect(self.onChanged_1_p_3)
        self.qle_to_m_1_p_3.clicked.connect(
            lambda i="st_l1_3": self.set_param(i))

        self.qle_to_m_1_p_4 = extQLineEdit(self)
        self.qle_to_m_1_p_4.resize(100, 25)
        self.qle_to_m_1_p_4.move(300, 190)
        self.qle_to_m_1_p_4.textChanged[str].connect(self.onChanged_1_p_4)
        self.qle_to_m_1_p_4.clicked.connect(
            lambda i="st_l1_4": self.set_param(i))

        # self.lbl_to_m_2 = QLabel(self)
        # self.lbl_to_m_2.setText("Вал №2")
        # self.lbl_to_m_2.adjustSize()
        # self.lbl_to_m_2.move(450, 80)

        # self.qle_to_m_2_p_1 = extQLineEdit(self)
        # self.qle_to_m_2_p_1.resize(100, 25)
        # self.qle_to_m_2_p_1.move(450, 100)
        # self.qle_to_m_2_p_1.textChanged[str].connect(self.onChanged_2_p_1)
        # self.qle_to_m_2_p_1.clicked.connect(lambda i="st_l2_1": self.set_param(i))

        # self.qle_to_m_2_p_2 = extQLineEdit(self)
        # self.qle_to_m_2_p_2.resize(100, 25)
        # self.qle_to_m_2_p_2.move(450, 130)
        # self.qle_to_m_2_p_2.textChanged[str].connect(self.onChanged_2_p_2)
        # self.qle_to_m_2_p_2.clicked.connect(lambda i="st_l2_2": self.set_param(i))

        # self.qle_to_m_2_p_3 = extQLineEdit(self)
        # self.qle_to_m_2_p_3.resize(100, 25)
        # self.qle_to_m_2_p_3.move(450, 160)
        # self.qle_to_m_2_p_3.textChanged[str].connect(self.onChanged_2_p_3)
        # self.qle_to_m_2_p_3.clicked.connect(lambda i="st_l2_3": self.set_param(i))

        # self.qle_to_m_2_p_4 = extQLineEdit(self)
        # self.qle_to_m_2_p_4.resize(100, 25)
        # self.qle_to_m_2_p_4.move(450, 190)
        # self.qle_to_m_2_p_4.textChanged[str].connect(self.onChanged_2_p_4)
        # self.qle_to_m_2_p_4.clicked.connect(lambda i="st_l2_4": self.set_param(i))

        # self.lbl_to_m_3 = QLabel(self)
        # self.lbl_to_m_3.setText("Вал №3")
        # self.lbl_to_m_3.adjustSize()
        # self.lbl_to_m_3.move(600, 80)

        # self.qle_to_m_3_p_1 = extQLineEdit(self)
        # self.qle_to_m_3_p_1.resize(100, 25)
        # self.qle_to_m_3_p_1.move(600, 100)
        # self.qle_to_m_3_p_1.textChanged[str].connect(self.onChanged_3_p_1)
        # self.qle_to_m_3_p_1.clicked.connect(lambda i="st_l3_1": self.set_param(i))

        # self.qle_to_m_3_p_2 = extQLineEdit(self)
        # self.qle_to_m_3_p_2.resize(100, 25)
        # self.qle_to_m_3_p_2.move(600, 130)
        # self.qle_to_m_3_p_2.textChanged[str].connect(self.onChanged_3_p_2)
        # self.qle_to_m_3_p_2.clicked.connect(lambda i="st_l3_2": self.set_param(i))

        # self.qle_to_m_3_p_3 = extQLineEdit(self)
        # self.qle_to_m_3_p_3.resize(100, 25)
        # self.qle_to_m_3_p_3.move(600, 160)
        # self.qle_to_m_3_p_3.textChanged[str].connect(self.onChanged_3_p_3)
        # self.qle_to_m_3_p_3.clicked.connect(lambda i="st_l3_3": self.set_param(i))

        # self.qle_to_m_3_p_4 = extQLineEdit(self)
        # self.qle_to_m_3_p_4.resize(100, 25)
        # self.qle_to_m_3_p_4.move(600, 190)
        # self.qle_to_m_3_p_4.textChanged[str].connect(self.onChanged_3_p_4)
        # self.qle_to_m_3_p_4.clicked.connect(lambda i="st_l3_4": self.set_param(i))

        # self.lbl_to_m_4 = QLabel(self)
        # self.lbl_to_m_4.setText("Вал №4")
        # self.lbl_to_m_4.adjustSize()
        # self.lbl_to_m_4.move(750, 80)

        # self.qle_to_m_4_p_1 = extQLineEdit(self)
        # self.qle_to_m_4_p_1.resize(100, 25)
        # self.qle_to_m_4_p_1.move(750, 100)
        # self.qle_to_m_4_p_1.textChanged[str].connect(self.onChanged_4_p_1)
        # self.qle_to_m_4_p_1.clicked.connect(lambda i="st_l4_1": self.set_param(i))

        # self.qle_to_m_4_p_2 = extQLineEdit(self)
        # self.qle_to_m_4_p_2.resize(100, 25)
        # self.qle_to_m_4_p_2.move(750, 130)
        # self.qle_to_m_4_p_2.textChanged[str].connect(self.onChanged_4_p_2)
        # self.qle_to_m_4_p_2.clicked.connect(lambda i="st_l4_2": self.set_param(i))

        # self.qle_to_m_4_p_3 = extQLineEdit(self)
        # self.qle_to_m_4_p_3.resize(100, 25)
        # self.qle_to_m_4_p_3.move(750, 160)
        # self.qle_to_m_4_p_3.textChanged[str].connect(self.onChanged_4_p_3)
        # self.qle_to_m_4_p_3.clicked.connect(lambda i="st_l4_3": self.set_param(i))

        # self.qle_to_m_4_p_4 = extQLineEdit(self)
        # self.qle_to_m_4_p_4.resize(100, 25)
        # self.qle_to_m_4_p_4.move(750, 190)
        # self.qle_to_m_4_p_4.textChanged[str].connect(self.onChanged_4_p_4)
        # self.qle_to_m_4_p_4.clicked.connect(lambda i="st_l4_4": self.set_param(i))

        # self.lbl_to_m_5 = QLabel(self)
        # self.lbl_to_m_5.setText("Вал №5")
        # self.lbl_to_m_5.adjustSize()
        # self.lbl_to_m_5.move(900, 80)

        # self.qle_to_m_5_p_1 = extQLineEdit(self)
        # self.qle_to_m_5_p_1.resize(100, 25)
        # self.qle_to_m_5_p_1.move(900, 100)
        # self.qle_to_m_5_p_1.textChanged[str].connect(self.onChanged_5_p_1)
        # self.qle_to_m_5_p_1.clicked.connect(lambda i="st_l5_1": self.set_param(i))

        # self.qle_to_m_5_p_2 = extQLineEdit(self)
        # self.qle_to_m_5_p_2.resize(100, 25)
        # self.qle_to_m_5_p_2.move(900, 130)
        # self.qle_to_m_5_p_2.textChanged[str].connect(self.onChanged_5_p_2)
        # self.qle_to_m_5_p_2.clicked.connect(lambda i="st_l5_2": self.set_param(i))

        # self.qle_to_m_5_p_3 = extQLineEdit(self)
        # self.qle_to_m_5_p_3.resize(100, 25)
        # self.qle_to_m_5_p_3.move(900, 160)
        # self.qle_to_m_5_p_3.textChanged[str].connect(self.onChanged_5_p_3)
        # self.qle_to_m_5_p_3.clicked.connect(lambda i="st_l5_3": self.set_param(i))

        # self.qle_to_m_5_p_4 = extQLineEdit(self)
        # self.qle_to_m_5_p_4.resize(100, 25)
        # self.qle_to_m_5_p_4.move(900, 190)
        # self.qle_to_m_5_p_4.textChanged[str].connect(self.onChanged_5_p_4)
        # self.qle_to_m_5_p_4.clicked.connect(lambda i="st_l5_4": self.set_param(i))

        # self.lbl_to_m_6 = QLabel(self)
        # self.lbl_to_m_6.setText("Вал №6")
        # self.lbl_to_m_6.adjustSize()
        # self.lbl_to_m_6.move(1050, 80)

        # self.qle_to_m_6_p_1 = extQLineEdit(self)
        # self.qle_to_m_6_p_1.resize(100, 25)
        # self.qle_to_m_6_p_1.move(1050, 100)
        # self.qle_to_m_6_p_1.textChanged[str].connect(self.onChanged_6_p_1)
        # self.qle_to_m_6_p_1.clicked.connect(lambda i="st_l6_1": self.set_param(i))

        # self.qle_to_m_6_p_2 = extQLineEdit(self)
        # self.qle_to_m_6_p_2.resize(100, 25)
        # self.qle_to_m_6_p_2.move(1050, 130)
        # self.qle_to_m_6_p_2.textChanged[str].connect(self.onChanged_6_p_2)
        # self.qle_to_m_6_p_2.clicked.connect(lambda i="st_l6_2": self.set_param(i))

        # self.qle_to_m_6_p_3 = extQLineEdit(self)
        # self.qle_to_m_6_p_3.resize(100, 25)
        # self.qle_to_m_6_p_3.move(1050, 160)
        # self.qle_to_m_6_p_3.textChanged[str].connect(self.onChanged_6_p_3)
        # self.qle_to_m_6_p_3.clicked.connect(lambda i="st_l6_3": self.set_param(i))

        # self.qle_to_m_6_p_4 = extQLineEdit(self)
        # self.qle_to_m_6_p_4.resize(100, 25)
        # self.qle_to_m_6_p_4.move(1050, 190)
        # self.qle_to_m_6_p_4.textChanged[str].connect(self.onChanged_6_p_4)
        # self.qle_to_m_6_p_4.clicked.connect(lambda i="st_l6_4": self.set_param(i))

        y_from = 250

        self.lbl_from = QLabel(self)
        self.lbl_from.setText("Текущие настрйки на ПЛК")
        self.lbl_from.adjustSize()
        self.lbl_from.move(50, y_from+50)

        self.lbl_from_1 = QLabel(self)
        self.lbl_from_1.setText("Установка по скорости min")
        self.lbl_from_1.adjustSize()
        self.lbl_from_1.move(0, y_from+100)

        self.lbl_from_2 = QLabel(self)
        self.lbl_from_2.setText("Установка времени включения")
        self.lbl_from_2.adjustSize()
        self.lbl_from_2.move(0, y_from+130)

        self.lbl_from_3 = QLabel(self)
        self.lbl_from_3.setText("Установка времени фильтрации (срабатывания)")
        self.lbl_from_3.adjustSize()
        self.lbl_from_3.move(0, y_from+160)

        self.lbl_from_4 = QLabel(self)
        self.lbl_from_4.setText("Установка времени на отсутствие импульсов")
        self.lbl_from_4.adjustSize()
        self.lbl_from_4.move(0, y_from+190)

        self.lbl_from_m_1 = QLabel(self)
        self.lbl_from_m_1.setText("Вал №1")
        self.lbl_from_m_1.adjustSize()
        self.lbl_from_m_1.move(300, y_from+80)

        self.qle_from_m_1_p_1 = QLineEdit(self)
        self.qle_from_m_1_p_1.resize(100, 25)
        self.qle_from_m_1_p_1.move(300, y_from+100)
        self.qle_from_m_1_p_1.setReadOnly(1)

        self.qle_from_m_1_p_2 = QLineEdit(self)
        self.qle_from_m_1_p_2.resize(100, 25)
        self.qle_from_m_1_p_2.move(300, y_from+130)
        self.qle_from_m_1_p_2.setReadOnly(1)

        self.qle_from_m_1_p_3 = QLineEdit(self)
        self.qle_from_m_1_p_3.resize(100, 25)
        self.qle_from_m_1_p_3.move(300, y_from+160)
        self.qle_from_m_1_p_3.setReadOnly(1)

        self.qle_from_m_1_p_4 = QLineEdit(self)
        self.qle_from_m_1_p_4.resize(100, 25)
        self.qle_from_m_1_p_4.move(300, y_from+190)
        self.qle_from_m_1_p_4.setReadOnly(1)

        # self.lbl_from_m_2 = QLabel(self)
        # self.lbl_from_m_2.setText("Вал №2")
        # self.lbl_from_m_2.adjustSize()
        # self.lbl_from_m_2.move(450, y_from+80)

        # self.qle_from_m_2_p_1 = QLineEdit(self)
        # self.qle_from_m_2_p_1.resize(100, 25)
        # self.qle_from_m_2_p_1.move(450, y_from+100)
        # self.qle_from_m_2_p_1.setReadOnly(1)

        # self.qle_from_m_2_p_2 = QLineEdit(self)
        # self.qle_from_m_2_p_2.resize(100, 25)
        # self.qle_from_m_2_p_2.move(450, y_from+130)
        # self.qle_from_m_2_p_2.setReadOnly(1)

        # self.qle_from_m_2_p_3 = QLineEdit(self)
        # self.qle_from_m_2_p_3.resize(100, 25)
        # self.qle_from_m_2_p_3.move(450, y_from+160)
        # self.qle_from_m_2_p_3.setReadOnly(1)

        # self.qle_from_m_2_p_4 = QLineEdit(self)
        # self.qle_from_m_2_p_4.resize(100, 25)
        # self.qle_from_m_2_p_4.move(450, y_from+190)
        # self.qle_from_m_2_p_4.setReadOnly(1)

        # self.lbl_from_m_3 = QLabel(self)
        # self.lbl_from_m_3.setText("Вал №3")
        # self.lbl_from_m_3.adjustSize()
        # self.lbl_from_m_3.move(600, y_from+80)

        # self.qle_from_m_3_p_1 = QLineEdit(self)
        # self.qle_from_m_3_p_1.resize(100, 25)
        # self.qle_from_m_3_p_1.move(600, y_from+100)
        # self.qle_from_m_3_p_1.setReadOnly(1)

        # self.qle_from_m_3_p_2 = QLineEdit(self)
        # self.qle_from_m_3_p_2.resize(100, 25)
        # self.qle_from_m_3_p_2.move(600, y_from+130)
        # self.qle_from_m_3_p_2.setReadOnly(1)

        # self.qle_from_m_3_p_3 = QLineEdit(self)
        # self.qle_from_m_3_p_3.resize(100, 25)
        # self.qle_from_m_3_p_3.move(600, y_from+160)
        # self.qle_from_m_3_p_3.setReadOnly(1)

        # self.qle_from_m_3_p_4 = QLineEdit(self)
        # self.qle_from_m_3_p_4.resize(100, 25)
        # self.qle_from_m_3_p_4.move(600, y_from+190)
        # self.qle_from_m_3_p_4.setReadOnly(1)

        # self.lbl_from_m_4 = QLabel(self)
        # self.lbl_from_m_4.setText("Вал №4")
        # self.lbl_from_m_4.adjustSize()
        # self.lbl_from_m_4.move(750, y_from+80)

        # self.qle_from_m_4_p_1 = QLineEdit(self)
        # self.qle_from_m_4_p_1.resize(100, 25)
        # self.qle_from_m_4_p_1.move(750, y_from+100)
        # self.qle_from_m_4_p_1.setReadOnly(1)

        # self.qle_from_m_4_p_2 = QLineEdit(self)
        # self.qle_from_m_4_p_2.resize(100, 25)
        # self.qle_from_m_4_p_2.move(750, y_from+130)
        # self.qle_from_m_4_p_2.setReadOnly(1)

        # self.qle_from_m_4_p_3 = QLineEdit(self)
        # self.qle_from_m_4_p_3.resize(100, 25)
        # self.qle_from_m_4_p_3.move(750, y_from+160)
        # self.qle_from_m_4_p_3.setReadOnly(1)

        # self.qle_from_m_4_p_4 = QLineEdit(self)
        # self.qle_from_m_4_p_4.resize(100, 25)
        # self.qle_from_m_4_p_4.move(750, y_from+190)
        # self.qle_from_m_4_p_4.setReadOnly(1)

        # self.lbl_from_m_5 = QLabel(self)
        # self.lbl_from_m_5.setText("Вал №5")
        # self.lbl_from_m_5.adjustSize()
        # self.lbl_from_m_5.move(900, y_from+80)

        # self.qle_from_m_5_p_1 = QLineEdit(self)
        # self.qle_from_m_5_p_1.resize(100, 25)
        # self.qle_from_m_5_p_1.move(900, y_from+100)
        # self.qle_from_m_5_p_1.setReadOnly(1)

        # self.qle_from_m_5_p_2 = QLineEdit(self)
        # self.qle_from_m_5_p_2.resize(100, 25)
        # self.qle_from_m_5_p_2.move(900, y_from+130)
        # self.qle_from_m_5_p_2.setReadOnly(1)

        # self.qle_from_m_5_p_3 = QLineEdit(self)
        # self.qle_from_m_5_p_3.resize(100, 25)
        # self.qle_from_m_5_p_3.move(900, y_from+160)
        # self.qle_from_m_5_p_3.setReadOnly(1)

        # self.qle_from_m_5_p_4 = QLineEdit(self)
        # self.qle_from_m_5_p_4.resize(100, 25)
        # self.qle_from_m_5_p_4.move(900, y_from+190)
        # self.qle_from_m_5_p_4.setReadOnly(1)

        # self.lbl_from_m_6 = QLabel(self)
        # self.lbl_from_m_6.setText("Вал №6")
        # self.lbl_from_m_6.adjustSize()
        # self.lbl_from_m_6.move(1050, y_from+80)

        # self.qle_from_m_6_p_1 = QLineEdit(self)
        # self.qle_from_m_6_p_1.resize(100, 25)
        # self.qle_from_m_6_p_1.move(1050, y_from+100)
        # self.qle_from_m_6_p_1.setReadOnly(1)

        # self.qle_from_m_6_p_2 = QLineEdit(self)
        # self.qle_from_m_6_p_2.resize(100, 25)
        # self.qle_from_m_6_p_2.move(1050, y_from+130)
        # self.qle_from_m_6_p_2.setReadOnly(1)

        # self.qle_from_m_6_p_3 = QLineEdit(self)
        # self.qle_from_m_6_p_3.resize(100, 25)
        # self.qle_from_m_6_p_3.move(1050, y_from+160)
        # self.qle_from_m_6_p_3.setReadOnly(1)

        # self.qle_from_m_6_p_4 = QLineEdit(self)
        # self.qle_from_m_6_p_4.resize(100, 25)
        # self.qle_from_m_6_p_4.move(1050, y_from+190)
        # self.qle_from_m_6_p_4.setReadOnly(1)

        # self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        # self.setGeometry(100, 100, 1280, 600)
        self.setGeometry(100, 100, 600, 600)
        self.setWindowTitle("Настройки разрывной дробилки")

    def createConfig(self):
        """
        Create a config file
        """
        path = "settings.ini"
       
        # config = configparser.ConfigParser()
        config  = get_config(path)

        try:
            # Window_tooth_setting
            config.set("Settings", "val_set_off_speed_min_1", str(self.val_set_off_speed_min_1))
            config.set("Settings", "val_set_on_time_1", str(self.val_set_on_time_1))
            config.set("Settings", "val_set_time_actuation_1", str(self.val_set_time_actuation_1))
            config.set("Settings", "val_set_time_pulse_off_1", str(self.val_set_time_pulse_off_1))
        except:
            config.add_section("Settings")
            # Window_tooth_setting
            config.set("Settings", "val_set_off_speed_min_1", str(self.val_set_off_speed_min_1))
            config.set("Settings", "val_set_on_time_1", str(self.val_set_on_time_1))
            config.set("Settings", "val_set_time_actuation_1", str(self.val_set_time_actuation_1))
            config.set("Settings", "val_set_time_pulse_off_1", str(self.val_set_time_pulse_off_1))

        with open(path, "w") as config_file:
            config.write(config_file)

    def set_standart_data(self):
        self.val_set_off_speed_min_1 = 500
        self.val_set_on_time_1 = 0.5
        self.val_set_time_actuation_1 = 1
        self.val_set_time_pulse_off_1 = 1
        
        self.qle_to_m_1_p_1.setText(str(self.val_set_off_speed_min_1))
        self.qle_to_m_1_p_2.setText(str(self.val_set_on_time_1))
        self.qle_to_m_1_p_3.setText(str(self.val_set_time_actuation_1))
        self.qle_to_m_1_p_4.setText(str(self.val_set_time_pulse_off_1))

    def put_data_to_edit(self):
        if self.param == 'st_l1_1':
            self.qle_to_m_1_p_1.setText(self.numpad.value)
        if self.param == 'st_l1_2':
            self.qle_to_m_1_p_2.setText(self.numpad.value)
        if self.param == 'st_l1_3':
            self.qle_to_m_1_p_3.setText(self.numpad.value)
        if self.param == 'st_l1_4':
            self.qle_to_m_1_p_4.setText(self.numpad.value)

        if self.param == 'st_l2_1':
            self.qle_to_m_2_p_1.setText(self.numpad.value)
        if self.param == 'st_l2_2':
            self.qle_to_m_2_p_2.setText(self.numpad.value)
        if self.param == 'st_l2_3':
            self.qle_to_m_2_p_3.setText(self.numpad.value)
        if self.param == 'st_l2_4':
            self.qle_to_m_2_p_4.setText(self.numpad.value)

        if self.param == 'st_l3_1':
            self.qle_to_m_3_p_1.setText(self.numpad.value)
        if self.param == 'st_l3_2':
            self.qle_to_m_3_p_2.setText(self.numpad.value)
        if self.param == 'st_l3_3':
            self.qle_to_m_3_p_3.setText(self.numpad.value)
        if self.param == 'st_l3_4':
            self.qle_to_m_3_p_4.setText(self.numpad.value)

        if self.param == 'st_l4_1':
            self.qle_to_m_4_p_1.setText(self.numpad.value)
        if self.param == 'st_l4_2':
            self.qle_to_m_4_p_2.setText(self.numpad.value)
        if self.param == 'st_l4_3':
            self.qle_to_m_4_p_3.setText(self.numpad.value)
        if self.param == 'st_l4_4':
            self.qle_to_m_4_p_4.setText(self.numpad.value)

        if self.param == 'st_l5_1':
            self.qle_to_m_5_p_1.setText(self.numpad.value)
        if self.param == 'st_l5_2':
            self.qle_to_m_5_p_2.setText(self.numpad.value)
        if self.param == 'st_l5_3':
            self.qle_to_m_5_p_3.setText(self.numpad.value)
        if self.param == 'st_l5_4':
            self.qle_to_m_5_p_4.setText(self.numpad.value)

        if self.param == 'st_l6_1':
            self.qle_to_m_6_p_1.setText(self.numpad.value)
        if self.param == 'st_l6_2':
            self.qle_to_m_6_p_2.setText(self.numpad.value)
        if self.param == 'st_l6_3':
            self.qle_to_m_6_p_3.setText(self.numpad.value)
        if self.param == 'st_l6_4':
            self.qle_to_m_6_p_4.setText(self.numpad.value)

    def set_param(self, param):
        self.param = param
        if self.param == 'st_l1_1':
            self.numpad.value = self.qle_to_m_1_p_1.text()
        if self.param == 'st_l1_2':
            self.numpad.value = self.qle_to_m_1_p_2.text()
        if self.param == 'st_l1_3':
            self.numpad.value = self.qle_to_m_1_p_3.text()
        if self.param == 'st_l1_4':
            self.numpad.value = self.qle_to_m_1_p_4.text()

        if self.param == 'st_l2_1':
            self.numpad.value = self.qle_to_m_2_p_1.text()
        if self.param == 'st_l2_2':
            self.numpad.value = self.qle_to_m_2_p_2.text()
        if self.param == 'st_l2_3':
            self.numpad.value = self.qle_to_m_2_p_3.text()
        if self.param == 'st_l2_4':
            self.numpad.value = self.qle_to_m_2_p_4.text()

        if self.param == 'st_l3_1':
            self.numpad.value = self.qle_to_m_3_p_1.text()
        if self.param == 'st_l3_2':
            self.numpad.value = self.qle_to_m_3_p_2.text()
        if self.param == 'st_l3_3':
            self.numpad.value = self.qle_to_m_3_p_3.text()
        if self.param == 'st_l3_4':
            self.numpad.value = self.qle_to_m_3_p_4.text()

        if self.param == 'st_l4_1':
            self.numpad.value = self.qle_to_m_4_p_1.text()
        if self.param == 'st_l4_2':
            self.numpad.value = self.qle_to_m_4_p_2.text()
        if self.param == 'st_l4_3':
            self.numpad.value = self.qle_to_m_4_p_3.text()
        if self.param == 'st_l4_4':
            self.numpad.value = self.qle_to_m_4_p_4.text()

        if self.param == 'st_l5_1':
            self.numpad.value = self.qle_to_m_5_p_1.text()
        if self.param == 'st_l5_2':
            self.numpad.value = self.qle_to_m_5_p_2.text()
        if self.param == 'st_l5_3':
            self.numpad.value = self.qle_to_m_5_p_3.text()
        if self.param == 'st_l5_4':
            self.numpad.value = self.qle_to_m_5_p_4.text()

        if self.param == 'st_l6_1':
            self.numpad.value = self.qle_to_m_6_p_1.text()
        if self.param == 'st_l6_2':
            self.numpad.value = self.qle_to_m_6_p_2.text()
        if self.param == 'st_l6_3':
            self.numpad.value = self.qle_to_m_6_p_3.text()
        if self.param == 'st_l6_4':
            self.numpad.value = self.qle_to_m_6_p_4.text()

    def show_NumPad(self):
        self.numpad.move(700, 500)
        self.numpad.show()

    def load_setting_data(self):
        print("load_satting")
        self.status_but = 1
        self.createConfig()

    def load_kay_bord(self):
        os.startfile(r'C:\Windows\System32\osk.exe')

    def onChanged_1_p_1(self, text):
        try:
            self.val_set_off_speed_min_1 = float(text)
            self.qle_to_m_1_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_1_p_2(self, text):
        try:
            self.val_set_on_time_1 = float(text)
            self.qle_to_m_1_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_1_p_3(self, text):
        try:
            self.val_set_time_actuation_1 = float(text)
            self.qle_to_m_1_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_1_p_4(self, text):
        try:
            self.val_set_time_pulse_off_1 = float(text)
            self.qle_to_m_1_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_1_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2_p_1(self, text):
        try:
            self.val_set_off_speed_min_2 = float(text)
            self.qle_to_m_2_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2_p_2(self, text):
        try:
            self.val_set_on_time_2 = float(text)
            self.qle_to_m_2_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2_p_3(self, text):
        try:
            self.val_set_time_actuation_2 = float(text)
            self.qle_to_m_2_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_2_p_4(self, text):
        try:
            self.val_set_time_pulse_off_2 = float(text)
            self.qle_to_m_2_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_2_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3_p_1(self, text):
        try:
            self.val_set_off_speed_min_3 = float(text)
            self.qle_to_m_3_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3_p_2(self, text):
        try:
            self.val_set_on_time_3 = float(text)
            self.qle_to_m_3_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3_p_3(self, text):
        try:
            self.val_set_time_actuation_3 = float(text)
            self.qle_to_m_3_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_3_p_4(self, text):
        try:
            self.val_set_time_pulse_off_3 = float(text)
            self.qle_to_m_3_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_3_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_4_p_1(self, text):
        try:
            self.val_set_off_speed_min_4 = float(text)
            self.qle_to_m_4_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_4_p_2(self, text):
        try:
            self.val_set_on_time_4 = float(text)
            self.qle_to_m_4_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_4_p_3(self, text):
        try:
            self.val_set_time_actuation_4 = float(text)
            self.qle_to_m_4_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_4_p_4(self, text):
        try:
            self.val_set_time_pulse_off_4 = float(text)
            self.qle_to_m_4_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_4_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_5_p_1(self, text):
        try:
            self.val_set_off_speed_min_5 = float(text)
            self.qle_to_m_5_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_5_p_2(self, text):
        try:
            self.val_set_on_time_5 = float(text)
            self.qle_to_m_5_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_5_p_3(self, text):
        try:
            self.val_set_time_actuation_5 = float(text)
            self.qle_to_m_5_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_5_p_4(self, text):
        try:
            self.val_set_time_pulse_off_5 = float(text)
            self.qle_to_m_5_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_5_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_6_p_1(self, text):
        try:
            self.val_set_off_speed_min_6 = float(text)
            self.qle_to_m_6_p_1.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_6_p_2(self, text):
        try:
            self.val_set_on_time_6 = float(text)
            self.qle_to_m_6_p_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_6_p_3(self, text):
        try:
            self.val_set_time_actuation_6 = float(text)
            self.qle_to_m_6_p_3.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_3.setStyleSheet('background : #ff4000; ')
            print("ValueError")

    def onChanged_6_p_4(self, text):
        try:
            self.val_set_time_pulse_off_6 = float(text)
            self.qle_to_m_6_p_4.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_to_m_6_p_4.setStyleSheet('background : #ff4000; ')
            print("ValueError")

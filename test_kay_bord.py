import sys
import os
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication, QMainWindow, QAction, qApp, QComboBox, QDialog)
from PyQt5.QtGui import QPixmap, QColor, QIcon, QFont
from PyQt5.QtCore import QCoreApplication, QTimer, QSize, QRect
import PyQt5.QtCore as QtCore
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal


class extQLineEdit(QLineEdit):
    clicked=pyqtSignal()
    def __init__(self,parent):
        QLineEdit.__init__(self,parent)
    def mousePressEvent(self,QMouseEvent):
        self.clicked.emit()


class MainWidget(QMainWindow):
    def __init__(self):
        super(MainWidget, self).__init__()
        self.setFixedSize(400,300)
        self.move(600,400)

        self.param = 0
        # self.value = ""
        self.numpad = NumPad()
        print(self.numpad.value)


        self.qle_1 = extQLineEdit(self)
        self.qle_1.move(20,20)
        self.qle_1.clicked.connect(lambda i="l1": self.set_param(i))
        self.qle_1.textChanged[str].connect(self.onChanged_1)

        self.qle_2 = extQLineEdit(self)
        self.qle_2.move(20,70)
        self.qle_2.clicked.connect(lambda i="l2": self.set_param(i))


        self.btn_key_board = QPushButton('', self)
        self.btn_key_board.clicked.connect(self.show_NumPad)
        self.btn_key_board.setIcon(QIcon('img/kay_bord.png'))
        self.btn_key_board.setToolTip('Включить клавиатуру')
        self.btn_key_board.setIconSize(QSize(45, 45))
        self.btn_key_board.resize(50, 50)
        self.btn_key_board.move(300, 200)


        self.numpad.btn_1.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_2.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_3.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_4.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_5.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_6.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_7.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_8.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_9.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_0.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_point.clicked.connect(self.put_data_to_edit)
        self.numpad.btn_backspace.clicked.connect(self.put_data_to_edit)

        # self.timer = QTimer(self)
        # self.timer.timeout.connect(self.put_data_to_edit)
        # self.timer.start(100)


    def onChanged_1(self, text):
        try:
            self.time_off_hammer = float(text)
            self.qle_1.setStyleSheet('background : #ffffff; ')
            # self.time_off_hammer_status = 1
        except ValueError:
            self.qle_1.setStyleSheet('background : #ff4000; ')
            # self.time_off_hammer_status = 0
            print("ValueError")

    def put_data_to_edit(self):
        if self.param == 'l1':
            self.qle_1.setText(self.numpad.value)
        if self.param == 'l2':
            self.qle_2.setText(self.numpad.value)


    def set_param(self, param):
        self.param = param
        if self.param == 'l1':
            self.numpad.value = self.qle_1.text()
        if self.param == 'l2':
            self.numpad.value = self.qle_2.text()



    def show_NumPad(self):
        self.numpad.move(700,500)
        self.numpad.show()




class NumPad(QDialog):
    def __init__(self):
        super().__init__()
        self.setupUi()

        self.value = ""

    def setupUi(self):
        self.btn_1 = QPushButton(self)
        self.btn_1.setText("1")
        self.btn_1.move(0, 0)
        self.btn_1.resize(75, 75)
        self.btn_1.clicked.connect(self.btn_1_click)

        self.btn_2 = QPushButton(self)
        self.btn_2.setText("2")
        self.btn_2.move(75, 0)
        self.btn_2.resize(75, 75)
        self.btn_2.clicked.connect(self.btn_2_click)

        self.btn_3 = QPushButton(self)
        self.btn_3.setText("3")
        self.btn_3.move(150, 0)
        self.btn_3.resize(75, 75)
        self.btn_3.clicked.connect(self.btn_3_click)

        self.btn_4 = QPushButton(self)
        self.btn_4.setText("4")
        self.btn_4.move(0, 75)
        self.btn_4.resize(75, 75)
        self.btn_4.clicked.connect(self.btn_4_click)

        self.btn_5 = QPushButton(self)
        self.btn_5.setText("5")
        self.btn_5.move(75, 75)
        self.btn_5.resize(75, 75)
        self.btn_5.clicked.connect(self.btn_5_click)

        self.btn_6 = QPushButton(self)
        self.btn_6.setText("6")
        self.btn_6.move(150, 75)
        self.btn_6.resize(75, 75)
        self.btn_6.clicked.connect(self.btn_6_click)

        self.btn_7 = QPushButton(self)
        self.btn_7.setText("7")
        self.btn_7.move(0, 150)
        self.btn_7.resize(75, 75)
        self.btn_7.clicked.connect(self.btn_7_click)

        self.btn_8 = QPushButton(self)
        self.btn_8.setText("8")
        self.btn_8.move(75, 150)
        self.btn_8.resize(75, 75)
        self.btn_8.clicked.connect(self.btn_8_click)

        self.btn_9 = QPushButton(self)
        self.btn_9.setText("9")
        self.btn_9.move(150, 150)
        self.btn_9.resize(75, 75)
        self.btn_9.clicked.connect(self.btn_9_click)

        self.btn_0 = QPushButton(self)
        self.btn_0.setText("0")
        self.btn_0.move(75, 225)
        self.btn_0.resize(75, 75)
        self.btn_0.clicked.connect(self.btn_0_click)

        self.btn_point = QPushButton(self)
        self.btn_point.setText(".")
        self.btn_point.move(0, 225)
        self.btn_point.resize(75, 75)
        self.btn_point.clicked.connect(self.btn_point_click)

        self.btn_backspace = QPushButton(self)
        self.btn_backspace.setText("<-")
        self.btn_backspace.move(150, 225)
        self.btn_backspace.resize(75, 75)
        self.btn_backspace.clicked.connect(self.btn_backspace_click)

        self.setWindowFlag(QtCore.Qt.WindowStaysOnTopHint)
        self.setGeometry(100, 100, 225, 300)
        self.setWindowTitle("NumPad")


    def btn_1_click(self):
        self.value = self.value + "1"
        print(self.value)

    def btn_2_click(self):
        self.value = self.value + "2"
        print(self.value)

    def btn_3_click(self):
        self.value = self.value + "3"
        print(self.value)

    def btn_4_click(self):
        self.value = self.value + "4"
        print(self.value)

    def btn_5_click(self):
        self.value = self.value + "5"
        print(self.value)

    def btn_6_click(self):
        self.value = self.value + "6"
        print(self.value)

    def btn_7_click(self):
        self.value = self.value + "7"
        print(self.value)

    def btn_8_click(self):
        self.value = self.value + "8"
        print(self.value)

    def btn_9_click(self):
        self.value = self.value + "9"
        print(self.value)

    def btn_0_click(self):
        self.value = self.value + "0"
        print(self.value)

    def btn_point_click(self):
        self.value = self.value + "."
        print(self.value)

    def btn_backspace_click(self):
        self.value = self.value[0:-1]
        print(self.value)

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = MainWidget()
    ex.show()
    # ex = NumPad()
    # ex.show()

    # menus = MenuDemo()
    # ex = Main_Window()
    sys.exit(app.exec_())

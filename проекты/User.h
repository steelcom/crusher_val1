typedef struct
{
	R_TRIGtyp RTRIG;
	RStyp RS;
	BOOL _IMPVAR_43_3;
	BOOL _IMPVAR_43_4;
	BOOL IN;
	BOOL OUT;
}
PLC_PRGtyp;


/* Enum definitions */


/* The Prototypes */
void PLC_PRG(PLC_PRGtyp* inst);
BOOL PLC_PRGinit(PLC_PRGtyp* inst, BOOL bRetain);

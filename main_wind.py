#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import (QWidget, QLabel, QPushButton, QApplication,
                             QLineEdit, QApplication, QMainWindow, QAction, qApp, QSplashScreen)
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap, QColor, QIcon
from PyQt5.QtCore import QCoreApplication, QTimer, QSize, QThread

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient

from datetime import datetime, date, time

import setting_wind
import copy
import subprocess
import socket
import struct
import pickle
import time
import serial

# import vk_api
# from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
# from vk_api.utils import get_random_id
import json
import requests
import random

import configparser


# # your_group_token = "55dfe00e75f1f578ae925c160e3740e451eb6a3578e17e6cfe6ef87bb3deff615fbc5fa8bdc42697cb66c"
# your_group_token = "40426646524ac9c0a075fc54430729b7c4a927dd81a23f1440b3cfc64e67ed48f8de489223bad4d4540da"

# # your_group_id = 193216117
# your_group_id = 193218173

# vk_session = vk_api.VkApi(token=your_group_token)
# longpoll = VkBotLongPoll(vk_session, your_group_id)
# vk = vk_session.get_api()


# import read_data
# --->>> на дисплей
# <<<--- на контроллер



class Window_Ui(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

        self.data_init()

        self.last_time = 0

        self.status_connect_controller = 0

        self.get_settings()

    def get_settings(self):
        path = "settings.ini"

        config = configparser.ConfigParser()
        config.read(path)

        self.val_set_off_speed_min_1  = float(config.get("Settings", "val_set_off_speed_min_1"))
        self.val_set_on_time_1   = float(config.get("Settings", "val_set_on_time_1"))
        self.val_set_time_actuation_1   = float(config.get("Settings", "val_set_time_actuation_1"))
        self.val_set_time_pulse_off_1   = float(config.get("Settings", "val_set_time_pulse_off_1"))
        self.hammer_set_off_speed_min   = float(config.get("Settings", "hammer_set_off_speed_min"))
        self.hammer_set_speed_on   = float(config.get("Settings", "hammer_set_speed_on"))
        self.time_molot_off_crush   = float(config.get("Settings", "time_molot_off_crush"))
        self.time_on_conveyor_1   = float(config.get("Settings", "time_on_conveyor_1"))
        self.time_on_conveyor_2   = float(config.get("Settings", "time_on_conveyor_2"))
        self.time_off_conveyor_2   = float(config.get("Settings", "time_off_conveyor_2"))
        self.time_off_hammer    = float(config.get("Settings", "time_off_hammer"))
        self.time_direct_rotation    = float(config.get("Settings", "time_direct_rotation"))
        self.time_reverse_rotation    = float(config.get("Settings", "time_reverse_rotation"))  
        self.pause_to_stop    = float(config.get("Settings", "pause_to_stop"))

        self.unique_name    = config.get("Settings", "unique_name")

        print('+'*100)
        
        self.setting_web.qle_to_1.setText(self.unique_name)

        self.setting_tooth.qle_to_m_1_p_1.setText(str(self.val_set_off_speed_min_1))
        self.setting_tooth.qle_to_m_1_p_2.setText(str(self.val_set_on_time_1))
        self.setting_tooth.qle_to_m_1_p_3.setText(str(self.val_set_time_actuation_1))
        self.setting_tooth.qle_to_m_1_p_4.setText(str(self.val_set_time_pulse_off_1))

        self.setting_hammer.qle_to_1.setText(str(self.hammer_set_off_speed_min))
        self.setting_hammer.qle_to_2.setText(str(self.hammer_set_speed_on))
        self.setting_hammer.qle_to_3.setText(str(self.time_molot_off_crush))

        self.setting.qle_to_1.setText(str(self.time_on_conveyor_1))
        # self.qle_to_2.setText(str(self.))
        self.setting.qle_to_3.setText(str(self.time_on_conveyor_2))
        self.setting.qle_to_4.setText(str(self.time_off_conveyor_2))
        self.setting.qle_to_5.setText(str(self.time_off_hammer))
        # self.qle_to_6.setText(str(self.))
        self.setting.qle_to_7.setText(str(self.time_direct_rotation))
        self.setting.qle_to_8.setText(str(self.time_reverse_rotation))
        self.setting.qle_to_9.setText(str(self.pause_to_stop))

        self.setting.status_but = 1
        self.setting_tooth.status_but = 1
        self.setting_hammer.status_but = 1

    def data_init(self):

        # self.connect_modbus()

        # print(sys.executable)
        # subprocess.Popen([sys.executable, 'read_data.py', 'argzzz1', 'argzzz2'])

        # транспортер выкидной
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_1 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_1 = 2

        # транспортер подающий
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_2 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_2 = 2

        # молотковая дробилка
        # --->>>
        # вкл/выкл
        self.hammer_on_off = 0
        # авария по перегреву
        self.hammer_crash_overload = 2
        # скорость вращения
        self.hammer_speed = 0
        # сработка на отключение транспортера
        self.hammer_conveyor_off = 0
        # <<<---
        # установка скорости мин (отключение транспортера)
        self.hammer_set_off_speed_min = 0
        # установка скорости мин (отключение транспортера)
        self.hammer_set_speed_on = 0
        # установка времени выкл. молотковой авария
        self.time_molot_off_crush = 0

        # вал разрывной
        # --->>>
        # вращения прямое
        self.val_direction_1_p = 0
        # self.val_direction_2_p = 0
        # self.val_direction_3_p = 0
        # self.val_direction_4_p = 0
        # self.val_direction_5_p = 0
        # self.val_direction_6_p = 0

        # вращения обратное
        self.val_direction_1_o = 0
        # self.val_direction_2_o = 0
        # self.val_direction_3_o = 0
        # self.val_direction_4_o = 0
        # self.val_direction_5_o = 0
        # self.val_direction_6_o = 0

        # срабатывание токового реле
        self.val_current_relay_1 = 2
        # self.val_current_relay_2 = 2
        # self.val_current_relay_3 = 2
        # self.val_current_relay_4 = 2
        # self.val_current_relay_5 = 2
        # self.val_current_relay_6 = 2

        # срабатывание по датчику скорости
        self.val_sensor_speed_1 = 2
        # self.val_sensor_speed_2 = 2
        # self.val_sensor_speed_3 = 2
        # self.val_sensor_speed_4 = 2
        # self.val_sensor_speed_5 = 2
        # self.val_sensor_speed_6 = 2

        # перегрев электродвигателя
        self.overheating_motor_1 = 2
        # self.overheating_motor_2 = 2
        # self.overheating_motor_3 = 2
        # self.overheating_motor_4 = 2
        # self.overheating_motor_5 = 2
        # self.overheating_motor_6 = 2

        # зажатие вкл (вала)
        self.val_clamping_on_1 = 0
        # self.val_clamping_on_2 = 0
        # self.val_clamping_on_3 = 0
        # self.val_clamping_on_4 = 0
        # self.val_clamping_on_5 = 0
        # self.val_clamping_on_6 = 0

        # скорость вращения
        self.val_speed_1 = 0
        # self.val_speed_2 = 0
        # self.val_speed_3 = 0
        # self.val_speed_4 = 0
        # self.val_speed_5 = 0
        # self.val_speed_6 = 0

        # количество поданных реверсов
        self.val_quantity_revers_1 = 0
        # self.val_quantity_revers_2 = 0
        # self.val_quantity_revers_3 = 0
        # self.val_quantity_revers_4 = 0
        # self.val_quantity_revers_5 = 0
        # self.val_quantity_revers_6 = 0

        self.val_current_relay_1_write_status = 1
        self.val_sensor_speed_1_write_status = 1
        self.overheating_motor_1_write_status = 1
        self.val_clamping_on_1_write_status = 1

        self.val_current_relay_1_last_time = 0
        self.val_sensor_speed_1_last_time = 0
        self.overheating_motor_1_last_time = 0
        self.val_clamping_on_1_last_time = 0

        self.conveyor_current_relay_1_last_time = 0
        self.conveyor_current_relay_2_last_time = 0
        self.conveyor_current_relay_1_write_status = 1
        self.conveyor_current_relay_2_write_status = 1

        self.hammer_crash_overload_last_time = 0
        self.hammer_crash_overload_write_status = 1

        # <<<---
        # установка отключения по скорости min
        self.val_set_off_speed_min_1 = 0
        # self.val_set_off_speed_min_2 = 0
        # self.val_set_off_speed_min_3 = 0
        # self.val_set_off_speed_min_4 = 0
        # self.val_set_off_speed_min_5 = 0
        # self.val_set_off_speed_min_6 = 0

        # установка времени включения
        self.val_set_on_time_1 = 0
        # self.val_set_on_time_2 = 0
        # self.val_set_on_time_3 = 0
        # self.val_set_on_time_4 = 0
        # self.val_set_on_time_5 = 0
        # self.val_set_on_time_6 = 0

        # установка времени фильтрации(срабатывание)
        self.val_set_time_actuation_1 = 0
        # self.val_set_time_actuation_2 = 0
        # self.val_set_time_actuation_3 = 0
        # self.val_set_time_actuation_4 = 0
        # self.val_set_time_actuation_5 = 0
        # self.val_set_time_actuation_6 = 0

        # установка времени на отсутствие импульсов
        self.val_set_time_pulse_off_1 = 0
        # self.val_set_time_pulse_off_2 = 0
        # self.val_set_time_pulse_off_3 = 0
        # self.val_set_time_pulse_off_4 = 0
        # self.val_set_time_pulse_off_5 = 0
        # self.val_set_time_pulse_off_6 = 0

        # Настройки
        # <<<---
        # время вкл выкидного транспортера
        self.time_on_conveyor_1 = 0
        # время выкл выкидного транспортера
        self.time_off_conveyor_1 = 0
        # время вкл подающего транспортера
        self.time_on_conveyor_2 = 0
        # время выкл подающего транспортера
        self.time_off_conveyor_2 = 0
        # время выключения молотковой дробилки
        self.time_off_hammer = 0
        # предпусковой звонок
        self.pre_launch = 0
        # время прямого вращения
        self.time_direct_rotation = 0
        # время обратного вражения
        self.time_reverse_rotation = 0
        # пауза на остановку
        self.pause_to_stop = 0

        # переменные для анимации

        self.alfa1 = 0
        # self.alfa2 = 0
        # self.alfa3 = 0
        # self.alfa4 = 0
        # self.alfa5 = 0
        # self.alfa6 = 0

        self.alfa_hammer = 0

        self.alfa_conveyor_val_1 = 0
        self.alfa_conveyor_val_2 = 0

        self.test_x = 50

        self.auto_mode = 0
        self.hand_mode = 0
        self.none_mode = 0

        self.unique_name = ""
        

        # запуск потока
        # self.Test_mp_instance = Test_mp(mainwindow=self)
        # self.Test_mp_instance.start()

    def initUI(self):

        self.mode = QLabel(self)
        self.mode.setGeometry(650, 280, 700, 500)

        self.drob = QLabel(self)
        self.drob.setPixmap(QPixmap('img/fone1.png'))
        self.drob.setGeometry(0, 0, 1280, 800)

        # +++++ к фону
        self.drob1 = QLabel(self)
        self.drob1.setPixmap(QPixmap('img/crash_1.png'))
        self.drob1.setGeometry(450, 500, 200, 100)

        self.drob2 = QLabel(self)
        self.drob2.setPixmap(QPixmap('img/crash_2.png'))
        self.drob2.setGeometry(1150, 310, 100, 300)

        self.drob3 = QLabel(self)
        self.drob3.setPixmap(QPixmap('img/conv1.png'))
        self.drob3.setGeometry(300, 640, 350, 200)

        self.drob4 = QLabel(self)
        self.drob4.setPixmap(QPixmap('img/conv2.png'))
        self.drob4.setGeometry(925, 660, 350, 200)

        # объекты для анимации
        self.tooth1 = QLabel(self)
        self.tooth1.setPixmap(QPixmap('img/зуб2/0.png'))
        self.tooth1.setGeometry(25, 100, 75, 75)

        # self.tooth2 = QLabel(self)
        # self.tooth2.setPixmap(QPixmap('img/зуб/0.png'))
        # self.tooth2.setGeometry(25, 175, 75, 75)

        # self.tooth3 = QLabel(self)
        # self.tooth3.setPixmap(QPixmap('img/зуб2/0.png'))
        # self.tooth3.setGeometry(25, 250, 75, 75)

        # self.tooth4 = QLabel(self)
        # self.tooth4.setPixmap(QPixmap('img/зуб/0.png'))
        # self.tooth4.setGeometry(25, 325, 75, 75)

        # self.tooth5 = QLabel(self)
        # self.tooth5.setPixmap(QPixmap('img/зуб2/0.png'))
        # self.tooth5.setGeometry(25, 400, 75, 75)

        # self.tooth6 = QLabel(self)
        # self.tooth6.setPixmap(QPixmap('img/зуб/0.png'))
        # self.tooth6.setGeometry(25, 475, 75, 75)

        self.tooth_carrent_1 = QLabel(self)
        self.tooth_carrent_1.setPixmap(QPixmap('img/carrent_0.png'))
        # self.tooth_carrent_1.resize(50, 50)
        self.tooth_carrent_1.setGeometry(100, 100, 75, 75)

        # self.tooth_carrent_2 = QLabel(self)
        # self.tooth_carrent_2.setPixmap(QPixmap('img/carrent_0.png'))
        # self.tooth_carrent_2.setGeometry(100, 175, 75, 75)

        # self.tooth_carrent_3 = QLabel(self)
        # self.tooth_carrent_3.setPixmap(QPixmap('img/carrent_0.png'))
        # self.tooth_carrent_3.setGeometry(100, 250, 75, 75)

        # self.tooth_carrent_4 = QLabel(self)
        # self.tooth_carrent_4.setPixmap(QPixmap('img/carrent_0.png'))
        # self.tooth_carrent_4.setGeometry(100, 325, 75, 75)

        # self.tooth_carrent_5 = QLabel(self)
        # self.tooth_carrent_5.setPixmap(QPixmap('img/carrent_0.png'))
        # self.tooth_carrent_5.setGeometry(100, 400, 75, 75)

        # self.tooth_carrent_6 = QLabel(self)
        # self.tooth_carrent_6.setPixmap(QPixmap('img/carrent_0.png'))
        # self.tooth_carrent_6.setGeometry(100, 475, 75, 75)

#       скорость
        self.tooth_speed_1 = QLabel(self)
        self.tooth_speed_1.setPixmap(QPixmap('img/speed_0.png'))
        self.tooth_speed_1.setGeometry(175, 100, 75, 75)

        # self.tooth_speed_2 = QLabel(self)
        # self.tooth_speed_2.setPixmap(QPixmap('img/speed_0.png'))
        # self.tooth_speed_2.setGeometry(175, 175, 75, 75)

        # self.tooth_speed_3 = QLabel(self)
        # self.tooth_speed_3.setPixmap(QPixmap('img/speed_0.png'))
        # self.tooth_speed_3.setGeometry(175, 250, 75, 75)

        # self.tooth_speed_4 = QLabel(self)
        # self.tooth_speed_4.setPixmap(QPixmap('img/speed_0.png'))
        # self.tooth_speed_4.setGeometry(175, 325, 75, 75)

        # self.tooth_speed_5 = QLabel(self)
        # self.tooth_speed_5.setPixmap(QPixmap('img/speed_0.png'))
        # self.tooth_speed_5.setGeometry(175, 400, 75, 75)

        # self.tooth_speed_6 = QLabel(self)
        # self.tooth_speed_6.setPixmap(QPixmap('img/speed_0.png'))
        # self.tooth_speed_6.setGeometry(175, 475, 75, 75)

#       температура
        self.tooth_temp_1 = QLabel(self)
        self.tooth_temp_1.setPixmap(QPixmap('img/temp_0.png'))
        self.tooth_temp_1.setGeometry(250, 100, 75, 75)

        # self.tooth_temp_2 = QLabel(self)
        # self.tooth_temp_2.setPixmap(QPixmap('img/temp_0.png'))
        # self.tooth_temp_2.setGeometry(250, 175, 75, 75)

        # self.tooth_temp_3 = QLabel(self)
        # self.tooth_temp_3.setPixmap(QPixmap('img/temp_0.png'))
        # self.tooth_temp_3.setGeometry(250, 250, 75, 75)

        # self.tooth_temp_4 = QLabel(self)
        # self.tooth_temp_4.setPixmap(QPixmap('img/temp_0.png'))
        # self.tooth_temp_4.setGeometry(250, 325, 75, 75)

        # self.tooth_temp_5 = QLabel(self)
        # self.tooth_temp_5.setPixmap(QPixmap('img/temp_0.png'))
        # self.tooth_temp_5.setGeometry(250, 400, 75, 75)

        # self.tooth_temp_6 = QLabel(self)
        # self.tooth_temp_6.setPixmap(QPixmap('img/temp_0.png'))
        # self.tooth_temp_6.setGeometry(250, 475, 75, 75)

        self.tooth_clamping_1 = QLabel(self)
        self.tooth_clamping_1.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        self.tooth_clamping_1.setGeometry(25, 100, 75, 75)

        # self.tooth_clamping_2 = QLabel(self)
        # self.tooth_clamping_2.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        # self.tooth_clamping_2.setGeometry(25, 175, 75, 75)

        # self.tooth_clamping_3 = QLabel(self)
        # self.tooth_clamping_3.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        # self.tooth_clamping_3.setGeometry(25, 250, 75, 75)

        # self.tooth_clamping_4 = QLabel(self)
        # self.tooth_clamping_4.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        # self.tooth_clamping_4.setGeometry(25, 325, 75, 75)

        # self.tooth_clamping_5 = QLabel(self)
        # self.tooth_clamping_5.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        # self.tooth_clamping_5.setGeometry(25, 400, 75, 75)

        # self.tooth_clamping_6 = QLabel(self)
        # self.tooth_clamping_6.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        # self.tooth_clamping_6.setGeometry(25, 475, 75, 75)

        self.lbl_tooth_speed_1 = QLabel(self)
        self.lbl_tooth_speed_1.setText("Скорость")
        self.lbl_tooth_speed_1.adjustSize()
        self.lbl_tooth_speed_1.move(325, 80)
        self.lbl_tooth_speed_2 = QLabel(self)
        self.lbl_tooth_speed_2.setText("вращения")
        self.lbl_tooth_speed_2.adjustSize()
        self.lbl_tooth_speed_2.move(325, 90)

        self.lbl_tooth_revers_1 = QLabel(self)
        self.lbl_tooth_revers_1.setText("Количество")
        self.lbl_tooth_revers_1.adjustSize()
        self.lbl_tooth_revers_1.move(400, 80)
        self.lbl_tooth_revers_2 = QLabel(self)
        self.lbl_tooth_revers_2.setText("реверсов")
        self.lbl_tooth_revers_2.adjustSize()
        self.lbl_tooth_revers_2.move(400, 90)

        self.qle_motor_speed_1 = QLineEdit(self)
        self.qle_motor_speed_1.resize(50, 25)
        self.qle_motor_speed_1.move(325, 125)
        self.qle_motor_speed_1.setReadOnly(1)

        self.qle_revers_1 = QLineEdit(self)
        self.qle_revers_1.resize(50, 25)
        self.qle_revers_1.move(400, 125)
        self.qle_revers_1.setReadOnly(1)

        # self.qle_motor_speed_2 = QLineEdit(self)
        # self.qle_motor_speed_2.resize(50, 25)
        # self.qle_motor_speed_2.move(325, 200)
        # self.qle_motor_speed_2.setReadOnly(1)

        # self.qle_revers_2 = QLineEdit(self)
        # self.qle_revers_2.resize(50, 25)
        # self.qle_revers_2.move(400, 200)
        # self.qle_revers_2.setReadOnly(1)

        # self.qle_motor_speed_3 = QLineEdit(self)
        # self.qle_motor_speed_3.resize(50, 25)
        # self.qle_motor_speed_3.move(325, 275)
        # self.qle_motor_speed_3.setReadOnly(1)

        # self.qle_revers_3 = QLineEdit(self)
        # self.qle_revers_3.resize(50, 25)
        # self.qle_revers_3.move(400, 275)
        # self.qle_revers_3.setReadOnly(1)

        # self.qle_motor_speed_4 = QLineEdit(self)
        # self.qle_motor_speed_4.resize(50, 25)
        # self.qle_motor_speed_4.move(325, 350)
        # self.qle_motor_speed_4.setReadOnly(1)

        # self.qle_revers_4 = QLineEdit(self)
        # self.qle_revers_4.resize(50, 25)
        # self.qle_revers_4.move(400, 350)
        # self.qle_revers_4.setReadOnly(1)

        # self.qle_motor_speed_5 = QLineEdit(self)
        # self.qle_motor_speed_5.resize(50, 25)
        # self.qle_motor_speed_5.move(325, 425)
        # self.qle_motor_speed_5.setReadOnly(1)

        # self.qle_revers_5 = QLineEdit(self)
        # self.qle_revers_5.resize(50, 25)
        # self.qle_revers_5.move(400, 425)
        # self.qle_revers_5.setReadOnly(1)

        # self.qle_motor_speed_6 = QLineEdit(self)
        # self.qle_motor_speed_6.resize(50, 25)
        # self.qle_motor_speed_6.move(325, 500)
        # self.qle_motor_speed_6.setReadOnly(1)

        # self.qle_revers_6 = QLineEdit(self)
        # self.qle_revers_6.resize(50, 25)
        # self.qle_revers_6.move(400, 500)
        # self.qle_revers_6.setReadOnly(1)

        # анимация молотковой дробилки
        self.hammer = QLabel(self)
        self.hammer.setPixmap(QPixmap('img/молотковое_колесо/0.png'))
        self.hammer.setGeometry(700, 100, 80, 80)

        # окна визуализации параметров

        self.lbl_hammer_speed_1 = QLabel(self)
        self.lbl_hammer_speed_1.setText("Скорость")
        self.lbl_hammer_speed_1.adjustSize()
        self.lbl_hammer_speed_1.move(875, 80)
        self.lbl_hammer_speed_2 = QLabel(self)
        self.lbl_hammer_speed_2.setText("вращения")
        self.lbl_hammer_speed_2.adjustSize()
        self.lbl_hammer_speed_2.move(875, 90)

        self.qle_motor_speed_hammer = QLineEdit(self)
        self.qle_motor_speed_hammer.resize(50, 25)
        self.qle_motor_speed_hammer.move(875, 125)
        self.qle_motor_speed_hammer.setReadOnly(1)

        self.hammer_carrent_over_load = QLabel(self)
        self.hammer_carrent_over_load.setPixmap(QPixmap('img/over_load_0.png'))
        self.hammer_carrent_over_load.setGeometry(800, 115, 50, 50)

        # объкт анимации вала транспортера 1
        self.conveyor_val_1 = QLabel(self)
        self.conveyor_val_1.setPixmap(QPixmap('img/вал_транспортера/0.png'))
        self.conveyor_val_1.setGeometry(37, 650, 75, 75)

        self.conveyor_val_carrent_1 = QLabel(self)
        self.conveyor_val_carrent_1.setPixmap(QPixmap('img/carrent_0.png'))
        self.conveyor_val_carrent_1.setGeometry(100, 650, 75, 75)

        # объкт анимации вала транспортера 2
        self.conveyor_val_2 = QLabel(self)
        self.conveyor_val_2.setPixmap(QPixmap('img/вал_транспортера/0.png'))
        self.conveyor_val_2.setGeometry(700, 650, 75, 75)

        self.conveyor_val_carrent_2 = QLabel(self)
        self.conveyor_val_carrent_2.setPixmap(QPixmap('img/carrent_0.png'))
        self.conveyor_val_carrent_2.setGeometry(765, 650, 75, 75)


        # кнопки настроек
        self.qbtn_1_m = QPushButton('', self)
        self.qbtn_1_m.clicked.connect(self.window_setting_open)
        self.qbtn_1_m.setIcon(QIcon('img/setting.png'))
        self.qbtn_1_m.setToolTip('Настройки')
        self.qbtn_1_m.setIconSize(QSize(45, 45))
        self.qbtn_1_m.resize(50, 50)
        self.qbtn_1_m.move(0, 0)

        self.setting = setting_wind.Window_setting(self)
        # self.setting.qle_to_2.setText("test")
        # self.setting.qle_from_1.setReadOnly(1)

        self.qbtn_serial = QPushButton('', self)
        self.qbtn_serial.clicked.connect(self.window_connect_com_port_open)
        self.qbtn_serial.setIcon(QIcon('img/connect_img.png'))
        self.qbtn_serial.setToolTip('Настройки COM порта')
        self.qbtn_serial.setIconSize(QSize(45, 45))
        self.qbtn_serial.resize(50, 50)
        self.qbtn_serial.move(50, 0)

        self.setting_com_port = setting_wind.Window_com_port(self)

        self.qbtn_web = QPushButton('', self)
        self.qbtn_web.clicked.connect(self.window_setting_web_open)
        self.qbtn_web.setIcon(QIcon('img/web_img.png'))
        self.qbtn_web.setToolTip('Настройки для отправки данных на сервер')
        self.qbtn_web.setIconSize(QSize(45, 45))
        self.qbtn_web.resize(50, 50)
        self.qbtn_web.move(100, 0)

        self.setting_web = setting_wind.Window_web(self)

        self.qbtn_2_m = QPushButton('', self)
        self.qbtn_2_m.clicked.connect(self.window_setting_tooth_open)
        self.qbtn_2_m.setIcon(QIcon('img/setting.png'))
        self.qbtn_2_m.setToolTip('Настройка параметров разрывной дробилки')
        self.qbtn_2_m.setIconSize(QSize(45, 45))
        self.qbtn_2_m.resize(50, 50)
        self.qbtn_2_m.move(590, 0)

        self.setting_tooth = setting_wind.Window_tooth_setting(self)

        self.qbtn_3_m = QPushButton('', self)
        self.qbtn_3_m.clicked.connect(self.window_setting_hammer_open)
        self.qbtn_3_m.setIcon(QIcon('img/setting.png'))
        self.qbtn_3_m.setToolTip('Настройка параметров молотковой дробилки')
        self.qbtn_3_m.setIconSize(QSize(45, 45))
        self.qbtn_3_m.resize(50, 50)
        self.qbtn_3_m.move(1230, 0)

        self.setting_hammer = setting_wind.Window_setting_hammer(self)

        self.setGeometry(100, 100, 1280, 800)
        self.setWindowTitle('Modbus')
        self.show()

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.animation)
        self.timer.start(100)

        # настроека таймера
        self.timer2 = QTimer(self)
        # self.timer2.timeout.connect(self.read_modbus)
        self.timer2.timeout.connect(self.put_data)
        self.timer2.start(100)

        # self.timer3 = QTimer(self)
        # self.timer3.timeout.connect(self.put_data_VK)
        # self.timer3.start(5000)

    def createConfig(self):
        """
        Create a config file
        """
        path = "settings.ini"
       
        config = configparser.ConfigParser()
        config.add_section("Settings")
         # Window_setting
        config.set("Settings", "time_on_conveyor_1", self.time_on_conveyor_1)
        config.set("Settings", "time_on_conveyor_2", self.time_on_conveyor_2)
        config.set("Settings", "time_off_conveyor_2", self.time_off_conveyor_2)
        config.set("Settings", "time_off_hammer", self.time_off_hammer)
        config.set("Settings", "time_direct_rotation", self.time_direct_rotation)
        config.set("Settings", "time_reverse_rotation", self.time_reverse_rotation)
        config.set("Settings", "pause_to_stop", self.pause_to_stop)
 
        # Window_setting_hammer
        config.set("Settings", "hammer_set_off_speed_min", self.hammer_set_off_speed_min)
        config.set("Settings", "hammer_set_speed_on", self.hammer_set_speed_on)
        config.set("Settings", "time_molot_off_crush", self.time_molot_off_crush)
        
        # Window_tooth_setting
        config.set("Settings", "val_set_off_speed_min_1", self.val_set_off_speed_min_1)
        config.set("Settings", "val_set_on_time_1", self.val_set_on_time_1)
        config.set("Settings", "val_set_time_actuation_1", self.val_set_time_actuation_1)
        config.set("Settings", "val_set_time_pulse_off_1", self.val_set_time_pulse_off_1)

        with open(path, "w") as config_file:
            config.write(config_file)

    def animation_tooth(self, p, o, tooth, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/зуб2/' + str(alfa) + '.png'
        tooth.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_tooth2(self, p, o, tooth, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/зуб/' + str(alfa) + '.png'
        tooth.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_hammer(self, p, o, hammer, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/молотковое_колесо/' + str(alfa) + '.png'
        hammer.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_conveyor_val(self, p, o, hammer, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'img/вал_транспортера/' + str(alfa) + '.png'
        hammer.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation(self):

        if (self.auto_mode == 1):
            self.mode.setPixmap(QPixmap('img/auto_mode.png'))
            # self.mode.resize(100,100)


        if (self.hand_mode == 1):
            self.mode.setPixmap(QPixmap('img/hand_mode.png'))



        if (self.none_mode == 1):
            self.mode.setPixmap(QPixmap('img/clamping_tooth_1.png'))
        # else:
        #     self.mode.setPixmap(QPixmap('img/clamping_tooth_1.png'))

        # тестовая проверка вращения зубьев
        self.alfa1 = self.animation_tooth(
            self.val_direction_1_p, self.val_direction_1_o, self.tooth1, self.alfa1)
        # self.alfa2 = self.animation_tooth2(
        #     self.val_direction_2_p, self.val_direction_2_o, self.tooth2, self.alfa2)
        # self.alfa3 = self.animation_tooth(
        #     self.val_direction_3_p, self.val_direction_3_o, self.tooth3, self.alfa3)
        # self.alfa4 = self.animation_tooth2(
        #     self.val_direction_4_p, self.val_direction_4_o, self.tooth4, self.alfa4)
        # self.alfa5 = self.animation_tooth(
        #     self.val_direction_5_p, self.val_direction_5_o, self.tooth5, self.alfa5)
        # self.alfa6 = self.animation_tooth2(
        #     self.val_direction_6_p, self.val_direction_6_o, self.tooth6, self.alfa6)

        self.alfa_hammer = self.animation_hammer(
            0, self.hammer_on_off, self.hammer, self.alfa_hammer)

        self.alfa_conveyor_val_1 = self.animation_conveyor_val(
            self.conveyor_on_off_1, 0, self.conveyor_val_1, self.alfa_conveyor_val_1)

        self.alfa_conveyor_val_2 = self.animation_conveyor_val(
            self.conveyor_on_off_2, 0, self.conveyor_val_2, self.alfa_conveyor_val_2)

        if (self.val_current_relay_1 == 1):
            self.tooth_carrent_1.setPixmap(QPixmap("img/carrent_2.png"))

            if(self.val_current_relay_1_write_status == 1 or time.time() >= self.val_current_relay_1_last_time + 30):
                f = open('log_data.txt', 'a')
                f.write('\n')
                f.write(str(datetime.now()) + '    ' +
                        'Сработал датчик тока мотора №1')
                f.close()
                self.val_current_relay_1_last_time = time.time()
                self.val_current_relay_1_write_status = 0

        elif(self.val_current_relay_1 == 0):
            self.tooth_carrent_1.setPixmap(QPixmap("img/carrent_1.png"))
            self.val_current_relay_1_write_status = 1
        elif(self.val_current_relay_1 == 2):
            self.tooth_carrent_1.setPixmap(QPixmap("img/carrent_0.png"))
            self.val_current_relay_1_write_status = 1

        # if (self.val_current_relay_2 == 1):
        #     self.tooth_carrent_2.setPixmap(QPixmap("img/carrent_2.png"))
        # elif(self.val_current_relay_2 == 0):
        #     self.tooth_carrent_2.setPixmap(QPixmap("img/carrent_1.png"))
        # elif(self.val_current_relay_2 == 2):
        #     self.tooth_carrent_2.setPixmap(QPixmap("img/carrent_0.png"))

        # if (self.val_current_relay_3 == 1):
        #     self.tooth_carrent_3.setPixmap(QPixmap("img/carrent_2.png"))
        # elif(self.val_current_relay_3 == 0):
        #     self.tooth_carrent_3.setPixmap(QPixmap("img/carrent_1.png"))
        # elif(self.val_current_relay_3 == 2):
        #     self.tooth_carrent_3.setPixmap(QPixmap("img/carrent_0.png"))

        # if (self.val_current_relay_4 == 1):
        #     self.tooth_carrent_4.setPixmap(QPixmap("img/carrent_2.png"))
        # elif(self.val_current_relay_4 == 0):
        #     self.tooth_carrent_4.setPixmap(QPixmap("img/carrent_1.png"))
        # elif(self.val_current_relay_4 == 2):
        #     self.tooth_carrent_4.setPixmap(QPixmap("img/carrent_0.png"))

        # if (self.val_current_relay_5 == 1):
        #     self.tooth_carrent_5.setPixmap(QPixmap("img/carrent_2.png"))
        # elif(self.val_current_relay_5 == 0):
        #     self.tooth_carrent_5.setPixmap(QPixmap("img/carrent_1.png"))
        # elif(self.val_current_relay_5 == 2):
        #     self.tooth_carrent_5.setPixmap(QPixmap("img/carrent_0.png"))

        # if (self.val_current_relay_6 == 1):
        #     self.tooth_carrent_6.setPixmap(QPixmap("img/carrent_2.png"))
        # elif(self.val_current_relay_6 == 0):
        #     self.tooth_carrent_6.setPixmap(QPixmap("img/carrent_1.png"))
        # elif(self.val_current_relay_6 == 2):
        #     self.tooth_carrent_6.setPixmap(QPixmap("img/carrent_0.png"))

        if (self.val_sensor_speed_1 == 1):
            self.tooth_speed_1.setPixmap(QPixmap("img/speed_2.png"))

            if(self.val_sensor_speed_1_write_status == 1 or time.time() >= self.val_sensor_speed_1_last_time + 30):
                f = open('log_data.txt', 'a')
                f.write('\n')
                f.write(str(datetime.now()) + '    ' +
                        'Сработал датчик скорости мотора №1')
                f.close()
                self.val_sensor_speed_1_last_time = time.time()
                self.val_sensor_speed_1_write_status = 0

        elif(self.val_sensor_speed_1 == 0):
            self.tooth_speed_1.setPixmap(QPixmap("img/speed_1.png"))
            self.val_sensor_speed_1_write_status = 1
        elif(self.val_sensor_speed_1 == 2):
            self.tooth_speed_1.setPixmap(QPixmap("img/speed_0.png"))
            self.val_sensor_speed_1_write_status = 1

        # if (self.val_sensor_speed_2 == 1):
        #     self.tooth_speed_2.setPixmap(QPixmap("img/speed_2.png"))
        # elif(self.val_sensor_speed_2 == 0):
        #     self.tooth_speed_2.setPixmap(QPixmap("img/speed_1.png"))
        # elif(self.val_sensor_speed_2 == 2):
        #     self.tooth_speed_2.setPixmap(QPixmap("img/speed_0.png"))

        # if (self.val_sensor_speed_3 == 1):
        #     self.tooth_speed_3.setPixmap(QPixmap("img/speed_2.png"))
        # elif(self.val_sensor_speed_3 == 0):
        #     self.tooth_speed_3.setPixmap(QPixmap("img/speed_1.png"))
        # elif(self.val_sensor_speed_3 == 2):
        #     self.tooth_speed_3.setPixmap(QPixmap("img/speed_0.png"))

        # if (self.val_sensor_speed_4 == 1):
        #     self.tooth_speed_4.setPixmap(QPixmap("img/speed_2.png"))
        # elif(self.val_sensor_speed_4 == 0):
        #     self.tooth_speed_4.setPixmap(QPixmap("img/speed_1.png"))
        # elif(self.val_sensor_speed_4 == 2):
        #     self.tooth_speed_4.setPixmap(QPixmap("img/speed_0.png"))

        # if (self.val_sensor_speed_5 == 1):
        #     self.tooth_speed_5.setPixmap(QPixmap("img/speed_2.png"))
        # elif(self.val_sensor_speed_5 == 0):
        #     self.tooth_speed_5.setPixmap(QPixmap("img/speed_1.png"))
        # elif(self.val_sensor_speed_5 == 2):
        #     self.tooth_speed_5.setPixmap(QPixmap("img/speed_0.png"))

        # if (self.val_sensor_speed_6 == 1):
        #     self.tooth_speed_6.setPixmap(QPixmap("img/speed_2.png"))
        # elif(self.val_sensor_speed_6 == 0):
        #     self.tooth_speed_6.setPixmap(QPixmap("img/speed_1.png"))
        # elif(self.val_sensor_speed_6 == 2):
        #     self.tooth_speed_6.setPixmap(QPixmap("img/speed_0.png"))

        if (self.overheating_motor_1 == 0):
            self.tooth_temp_1.setPixmap(QPixmap("img/temp_2.png"))

            if(self.overheating_motor_1_write_status == 1 or time.time() >= self.overheating_motor_1_last_time + 30):
                f = open('log_data.txt', 'a')
                f.write('\n')
                f.write(str(datetime.now()) + '    ' +
                        'Сработал датчик прегрева мотора №1')
                f.close()
                self.overheating_motor_1_last_time = time.time()
                self.overheating_motor_1_write_status = 0

        elif (self.overheating_motor_1 == 1):
            self.tooth_temp_1.setPixmap(QPixmap("img/temp_1.png"))
            self.overheating_motor_1_write_status = 1
        elif (self.overheating_motor_1 == 2):
            self.tooth_temp_1.setPixmap(QPixmap("img/temp_0.png"))
            self.overheating_motor_1_write_status = 1

        # if (self.overheating_motor_2 == 1):
        #     self.tooth_temp_2.setPixmap(QPixmap("img/temp_2.png"))
        # elif (self.overheating_motor_2 == 0):
        #     self.tooth_temp_2.setPixmap(QPixmap("img/temp_1.png"))
        # elif (self.overheating_motor_2 == 2):
        #     self.tooth_temp_2.setPixmap(QPixmap("img/temp_0.png"))

        # if (self.overheating_motor_3 == 1):
        #     self.tooth_temp_3.setPixmap(QPixmap("img/temp_2.png"))
        # elif (self.overheating_motor_3 == 0):
        #     self.tooth_temp_3.setPixmap(QPixmap("img/temp_1.png"))
        # elif (self.overheating_motor_3 == 2):
        #     self.tooth_temp_3.setPixmap(QPixmap("img/temp_0.png"))

        # if (self.overheating_motor_4 == 1):
        #     self.tooth_temp_4.setPixmap(QPixmap("img/temp_2.png"))
        # elif (self.overheating_motor_4 == 0):
        #     self.tooth_temp_4.setPixmap(QPixmap("img/temp_1.png"))
        # elif (self.overheating_motor_4 == 2):
        #     self.tooth_temp_4.setPixmap(QPixmap("img/temp_0.png"))

        # if (self.overheating_motor_5 == 1):
        #     self.tooth_temp_5.setPixmap(QPixmap("img/temp_2.png"))
        # elif (self.overheating_motor_5 == 0):
        #     self.tooth_temp_5.setPixmap(QPixmap("img/temp_1.png"))
        # elif (self.overheating_motor_5 == 2):
        #     self.tooth_temp_5.setPixmap(QPixmap("img/temp_0.png"))

        # if (self.overheating_motor_6 == 1):
        #     self.tooth_temp_6.setPixmap(QPixmap("img/temp_2.png"))
        # elif (self.overheating_motor_6 == 0):
        #     self.tooth_temp_6.setPixmap(QPixmap("img/temp_1.png"))
        # elif (self.overheating_motor_6 == 2):
        #     self.tooth_temp_6.setPixmap(QPixmap("img/temp_0.png"))

        # отображене зажатия
        if (self.val_clamping_on_1 == 1):
            self.tooth_clamping_1.setPixmap(
                QPixmap("img/clamping_tooth_2.png"))

            if(self.val_clamping_on_1_write_status == 1 or time.time() >= self.val_clamping_on_1_last_time + 30):
                f = open('log_data.txt', 'a')
                f.write('\n')
                f.write(str(datetime.now()) + '    ' + 'Зажатие вала №1')
                f.close()
                self.val_clamping_on_1_last_time = time.time()
                self.val_clamping_on_1_write_status = 0

        elif (self.val_clamping_on_1 == 0):
            self.tooth_clamping_1.setPixmap(
                QPixmap("img/clamping_tooth_1.png"))
            self.val_clamping_on_1_write_status = 1

        # if (self.val_clamping_on_2 == 1):
        #     self.tooth_clamping_2.setPixmap(
        #         QPixmap("img/clamping_tooth_2.png"))
        # elif (self.val_clamping_on_2 == 0):
        #     self.tooth_clamping_2.setPixmap(
        #         QPixmap("img/clamping_tooth_1.png"))

        # if (self.val_clamping_on_3 == 1):
        #     self.tooth_clamping_3.setPixmap(
        #         QPixmap("img/clamping_tooth_2.png"))
        # elif (self.val_clamping_on_3 == 0):
        #     self.tooth_clamping_3.setPixmap(
        #         QPixmap("img/clamping_tooth_1.png"))

        # if (self.val_clamping_on_4 == 1):
        #     self.tooth_clamping_4.setPixmap(
        #         QPixmap("img/clamping_tooth_2.png"))
        # elif (self.val_clamping_on_4 == 0):
        #     self.tooth_clamping_4.setPixmap(
        #         QPixmap("img/clamping_tooth_1.png"))

        # if (self.val_clamping_on_5 == 1):
        #     self.tooth_clamping_5.setPixmap(
        #         QPixmap("img/clamping_tooth_2.png"))
        # elif (self.val_clamping_on_5 == 0):
        #     self.tooth_clamping_5.setPixmap(
        #         QPixmap("img/clamping_tooth_1.png"))

        # if (self.val_clamping_on_6 == 1):
        #     self.tooth_clamping_6.setPixmap(
        #         QPixmap("img/clamping_tooth_2.png"))
        # elif (self.val_clamping_on_6 == 0):
        #     self.tooth_clamping_6.setPixmap(
        #         QPixmap("img/clamping_tooth_1.png"))

        # сработка токового реле конвеера 1 2
        if (self.conveyor_current_relay_1 == 1):
            self.conveyor_val_carrent_1.setPixmap(QPixmap("img/carrent_2.png"))

            if(self.conveyor_current_relay_1_write_status == 1 or time.time() >= self.conveyor_current_relay_1_last_time + 30):
                f = open('log_data.txt', 'a')
                f.write('\n')
                f.write(str(datetime.now()) + '    ' +
                        'Сработка токового реле выкидного транспортера')
                f.close()
                self.vconveyor_current_relay_1_last_time = time.time()
                self.conveyor_current_relay_1_write_status = 0

        elif (self.conveyor_current_relay_1 == 0):
            self.conveyor_val_carrent_1.setPixmap(QPixmap("img/carrent_1.png"))
            self.conveyor_current_relay_1_write_status = 1
        elif (self.conveyor_current_relay_1 == 2):
            self.conveyor_val_carrent_1.setPixmap(QPixmap("img/carrent_0.png"))
            self.conveyor_current_relay_1_write_status = 1

        if (self.conveyor_current_relay_2 == 1):
            self.conveyor_val_carrent_2.setPixmap(QPixmap("img/carrent_2.png"))

            if(self.conveyor_current_relay_2_write_status == 1 or time.time() >= self.conveyor_current_relay_2_last_time + 30):
                f = open('log_data.txt', 'a')
                f.write('\n')
                f.write(str(datetime.now()) + '    ' +
                        'Сработка токового реле подающего транспортера')
                f.close()
                self.conveyor_current_relay_2_last_time = time.time()
                self.conveyor_current_relay_2_write_status = 0

        elif (self.conveyor_current_relay_2 == 0):
            self.conveyor_val_carrent_2.setPixmap(QPixmap("img/carrent_1.png"))
            self.conveyor_current_relay_2_write_status = 1
        elif (self.conveyor_current_relay_2 == 2):
            self.conveyor_val_carrent_2.setPixmap(QPixmap("img/carrent_0.png"))
            self.conveyor_current_relay_2_write_status = 1

        #  срабощтка на перегрузку молотковой дробилки
        if (self.hammer_crash_overload == 1):
            self.hammer_carrent_over_load.setPixmap(
                QPixmap("img/over_load_2.png"))

            if(self.hammer_crash_overload_write_status == 1 or time.time() >= self.hammer_crash_overload_last_time + 30):
                f = open('log_data.txt', 'a')
                f.write('\n')
                f.write(str(datetime.now()) + '    ' +
                        'Сработка на перегрузку молотковой дробилки')
                f.close()
                self.hammer_crash_overload_last_time = time.time()
                self.hammer_crash_overload_write_status = 0

        elif (self.hammer_crash_overload == 0):
            self.hammer_carrent_over_load.setPixmap(
                QPixmap("img/over_load_1.png"))
            self.hammer_crash_overload_write_status = 1
        elif (self.hammer_crash_overload == 2):
            self.hammer_carrent_over_load.setPixmap(
                QPixmap("img/over_load_0.png"))
            self.hammer_crash_overload_write_status = 1

        # if (self.val_sensor_speed_6):
        #     self.tooth_speed_6.setPixmap(QPixmap("speed_2.png"))
        # else:
        #     self.tooth_speed_6.setPixmap(QPixmap("speed_1.png"))

    # def put_data_VK(self):
    #     peer_id = 2000000005
    #     # group_messages = vk.messages.getHistory(peer_id=peer_id)
    #     # last_message = group_messages.get(u'items')[0].get(u'text')
    #     # print(last_message)
    #     # text = last_message

    #     data = {
    #                 "val_current_relay_1": self.val_current_relay_1,
    #                 "val_sensor_speed_1": self.val_sensor_speed_1,
    #                 "overheating_motor_1": self.overheating_motor_1,
    #                 "val_clamping_on_1": self.val_clamping_on_1,
    #                 "val_speed_1": self.val_speed_1,
    #                 "val_quantity_revers_1": self.val_quantity_revers_1,

    #                 # "val_current_relay_2": self.val_current_relay_2,
    #                 # "val_sensor_speed_2": self.val_sensor_speed_2,
    #                 # "overheating_motor_2": self.overheating_motor_2,
    #                 # "val_clamping_on_2": self.val_clamping_on_2,
    #                 # "val_speed_2": self.val_speed_2,
    #                 # "val_quantity_revers_2": self.val_quantity_revers_2,

    #                 "hammer_on_off": self.hammer_on_off,
    #                 "hammer_crash_overload": self.hammer_crash_overload,
    #                 "hammer_speed": self.hammer_speed,
    #                 "hammer_conveyor_off": self.hammer_conveyor_off,

    #                 # // транспортер выкидной
    #                 "conveyor_on_off_1": self.conveyor_on_off_1,
    #                 "conveyor_current_relay_1": self.conveyor_current_relay_1,

    #                 # //  транспортер подающий
    #                 "conveyor_on_off_2": self.conveyor_on_off_2,
    #                 "conveyor_current_relay_2": self.conveyor_current_relay_2,
    #     }
    #     print(data)
    #     out = json.dumps(data)
    #     print(out)
    #     message = out
    #     vk.messages.send(peer_id=peer_id, message=message,
    #                      random_id=get_random_id())

    def put_data_to_SQL(self):

        data = {
            "gap": {
                "val1": {
                    "val_direction_1_p": self.val_direction_1_p, 
                    "val_direction_1_o": self.val_direction_1_o,
                     
                    "val_current_relay": self.val_current_relay_1,
                    "val_sensor_speed": self.val_sensor_speed_1,
                    "overheating_motor": self.overheating_motor_1,
                    "val_clamping_on": self.val_clamping_on_1,
                    "val_speed": self.val_speed_1,
                    "val_quantity_revers": self.val_quantity_revers_1
                }
                # ,
                # "val2": {
                #     "val_current_relay": self.val_current_relay_2,
                #     "val_sensor_speed": self.val_sensor_speed_2,
                #     "overheating_motor":self.overheating_motor_2,
                #     "val_clamping_on": self.val_clamping_on_2,
                #     "val_speed": self.val_speed_2,
                #     "val_quantity_revers": self.val_quantity_revers_2,
                # },
                # "val3": {
                #     "val_current_relay": self.val_current_relay_3,
                #     "val_sensor_speed": self.val_sensor_speed_3,
                #     "overheating_motor":self.overheating_motor_3,
                #     "val_clamping_on": self.val_clamping_on_3,
                #     "val_speed": self.val_speed_3,
                #     "val_quantity_revers": self.val_quantity_revers_3,
                # },
                # "val4": {
                #     "val_current_relay": self.val_current_relay_4,
                #     "val_sensor_speed": self.val_sensor_speed_4,
                #     "overheating_motor":self.overheating_motor_4,
                #     "val_clamping_on": self.val_clamping_on_4,
                #     "val_speed": self.val_speed_4,
                #     "val_quantity_revers": self.val_quantity_revers_4,
                # },
                # "val5": {
                #     "val_current_relay": self.val_current_relay_5,
                #     "val_sensor_speed": self.val_sensor_speed_5,
                #     "overheating_motor":self.overheating_motor_5,
                #     "val_clamping_on": self.val_clamping_on_5,
                #     "val_speed": self.val_speed_5,
                #     "val_quantity_revers": self.val_quantity_revers_5,
                # },
                # "val6": {
                #     "val_current_relay": self.val_current_relay_6,
                #     "val_sensor_speed": self.val_sensor_speed_6,
                #     "overheating_motor":self.overheating_motor_6,
                #     "val_clamping_on": self.val_clamping_on_6,
                #     "val_speed": self.val_speed_6,
                #     "val_quantity_revers": self.val_quantity_revers_6,
                # }
            },
            "hamer": {
                "hammer_on_off": self.hammer_on_off,
                "hammer_crash_overload":  self.hammer_crash_overload,
                "hammer_speed":  self.hammer_speed,
                "hammer_conveyor_off":  self.hammer_conveyor_off
            },
            "conveyor1": {
                "conveyor_on_off_1": self.conveyor_on_off_1,
                "conveyor_current_relay_1": self.conveyor_current_relay_1
            },
            "conveyor2": {
                "conveyor_on_off_2": self.conveyor_on_off_2,
                "conveyor_current_relay_2": self.conveyor_current_relay_2
            }
        }

        time_ = time.time()

        out = json.dumps(data)
        url = 'http://188.225.39.107/CRUSHER2/aggregate.php'
        x = requests.post(
            url, data={"action": "insert_new_data", "data": out, "time": time_, "table": self.unique_name })
        print(x.text)



    def run_all_mp(self):
        self.Test_mp_instance = Test_mp(mainwindow=self)
        self.Test_mp_instance.start()

        # self.VK_mp_instance = VK_mp(mainwindow_2=self)
        # self.VK_mp_instance.start()

        self.SQL_mp_instance = SQL_mp(mainwindow_3=self)
        self.SQL_mp_instance.start()

    #  прорисовка запись данных на главном окне

    def put_data(self):

        # connect_status_glob = self.setting_com_port.connect_status

        if(self.setting_com_port.connect_status and self.setting_com_port.connect_status_flag):
            # self.run_all_mp()
            if(time.time() - self.last_time >= 3600):
                self.run_all_mp()
                self.last_time = time.time()
            # self.Test_mp_instance = Test_mp(mainwindow=self)
            # self.Test_mp_instance.start()

            # self.VK_mp_instance = VK_mp(mainwindow_2=self)
            # self.VK_mp_instance.start()

            # self.SQL_mp_instance = SQL_mp(mainwindow_3=self)
            # self.SQL_mp_instance.start()

            self.setting_com_port.connect_status_flag = 0

        # print("put_data")
        # обработка параметров и вывод индикации

        # скорость вращения молотковой дробилки
        self.qle_motor_speed_hammer.setText(str(round(self.hammer_speed, 2)))

        # скорость вращения
        self.qle_motor_speed_1.setText(str(round(self.val_speed_1, 2)))
        # self.qle_motor_speed_2.setText(str(round(self.val_speed_2,2)))
        # self.qle_motor_speed_3.setText(str(round(self.val_speed_3,2)))
        # self.qle_motor_speed_4.setText(str(round(self.val_speed_4,2)))
        # self.qle_motor_speed_5.setText(str(round(self.val_speed_5,2)))
        # self.qle_motor_speed_6.setText(str(round(self.val_speed_6,2)))
        # количество поданных реверсов
        self.qle_revers_1.setText(str(round(self.val_quantity_revers_1, 2)))
        # self.qle_revers_2.setText(str(round(self.val_quantity_revers_2,2)))
        # self.qle_revers_3.setText(str(round(self.val_quantity_revers_3,2)))
        # self.qle_revers_4.setText(str(round(self.val_quantity_revers_4,2)))
        # self.qle_revers_5.setText(str(round(self.val_quantity_revers_5,2)))
        # self.qle_revers_6.setText(str(round(self.val_quantity_revers_6,2)))

        # Установка по скорости min
        self.setting_tooth.qle_from_m_1_p_1.setText(
            str(round(self.val_set_off_speed_min_1, 2)))
        # self.setting_tooth.qle_from_m_2_p_1.setText(str(round(self.val_set_off_speed_min_2,2)))
        # self.setting_tooth.qle_from_m_3_p_1.setText(str(round(self.val_set_off_speed_min_3,2)))
        # self.setting_tooth.qle_from_m_4_p_1.setText(str(round(self.val_set_off_speed_min_4,2)))
        # self.setting_tooth.qle_from_m_5_p_1.setText(str(round(self.val_set_off_speed_min_5,2)))
        # self.setting_tooth.qle_from_m_6_p_1.setText(str(round(self.val_set_off_speed_min_6,2)))

        # установка времени включения
        self.setting_tooth.qle_from_m_1_p_2.setText(
            str(round(self.val_set_on_time_1, 2)))
        # self.setting_tooth.qle_from_m_2_p_2.setText(str(round(self.val_set_on_time_2,2)))
        # self.setting_tooth.qle_from_m_3_p_2.setText(str(round(self.val_set_on_time_3,2)))
        # self.setting_tooth.qle_from_m_4_p_2.setText(str(round(self.val_set_on_time_4,2)))
        # self.setting_tooth.qle_from_m_5_p_2.setText(str(round(self.val_set_on_time_5,2)))
        # self.setting_tooth.qle_from_m_6_p_2.setText(str(round(self.val_set_on_time_6,2)))

        # установка времени фильтрации(срабатывание)
        self.setting_tooth.qle_from_m_1_p_3.setText(
            str(round(self.val_set_time_actuation_1, 2)))
        # self.setting_tooth.qle_from_m_2_p_3.setText(str(round(self.val_set_time_actuation_2,2)))
        # self.setting_tooth.qle_from_m_3_p_3.setText(str(round(self.val_set_time_actuation_3,2)))
        # self.setting_tooth.qle_from_m_4_p_3.setText(str(round(self.val_set_time_actuation_4,2)))
        # self.setting_tooth.qle_from_m_5_p_3.setText(str(round(self.val_set_time_actuation_5,2)))
        # self.setting_tooth.qle_from_m_6_p_3.setText(str(round(self.val_set_time_actuation_6,2)))

        # установка времени на отсутствие импульсов
        self.setting_tooth.qle_from_m_1_p_4.setText(
            str(round(self.val_set_time_pulse_off_1, 2)))
        # self.setting_tooth.qle_from_m_2_p_4.setText(str(round(self.val_set_time_pulse_off_2,2)))
        # self.setting_tooth.qle_from_m_3_p_4.setText(str(round(self.val_set_time_pulse_off_3,2)))
        # self.setting_tooth.qle_from_m_4_p_4.setText(str(round(self.val_set_time_pulse_off_4,2)))
        # self.setting_tooth.qle_from_m_5_p_4.setText(str(round(self.val_set_time_pulse_off_5,2)))
        # self.setting_tooth.qle_from_m_6_p_4.setText(str(round(self.val_set_time_pulse_off_6,2)))

        # установка скорости мин (отключение транспортера)
        self.setting_hammer.qle_from_1.setText(
            str(round(self.hammer_set_off_speed_min, 2)))
        # установка скорости мин (отключение транспортера)
        self.setting_hammer.qle_from_2.setText(
            str(round(self.hammer_set_speed_on, 2)))
        # время выключения молотковой по отсутствию импульсов
        self.setting_hammer.qle_from_3.setText(
            str(round(self.time_molot_off_crush, 2)))

        # отображение данных в окне настройки
        self.setting.qle_from_1.setText(str(round(self.time_on_conveyor_1, 2)))
        # self.setting.qle_from_2.setText(str(round(self.time_off_conveyor_1,2)))
        self.setting.qle_from_3.setText(str(round(self.time_on_conveyor_2, 2)))
        self.setting.qle_from_4.setText(
            str(round(self.time_off_conveyor_2, 2)))

        self.setting.qle_from_5.setText(str(round(self.time_off_hammer, 2)))
        # self.setting.qle_from_6.setText(str(round(self.pre_launch,2)))
        self.setting.qle_from_7.setText(
            str(round(self.time_direct_rotation, 2)))
        self.setting.qle_from_8.setText(
            str(round(self.time_reverse_rotation, 2)))
        self.setting.qle_from_9.setText(str(round(self.pause_to_stop, 2)))

        try:
            self.setting_com_port.connect_status = self.setting_com_port.client.connect()
            # print("-"*60)
        except Exception as e:
            pass
            print(e)

        if(not self.setting_com_port.connect_status and self.setting_com_port.flag_war_msg):
            self.data_init()

            msg = QMessageBox()
            msg.setWindowTitle("Warning")
            msg.setText("Соединение разорвано")
            # msg.setIcon(QMessageBox.Question)
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()

        if(self.status_connect_controller >= 20):
            self.data_init()

            msg = QMessageBox()
            msg.setWindowTitle("Warning")
            msg.setText("Контроллер выключен")
            # msg.setIcon(QMessageBox.Question)
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()
            self.status_connect_controller = 0

        if(not self.setting_com_port.connect_status and self.setting_com_port.flag_after_first_connect):
            # if(not self.setting_com_port.connect_status ):
            # self.client = self.setting_com_port.client
            # self.setting_com_port.client = ModbusSerialClient(method="rtu", port=self.setting_com_port.Port.currentText(), stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)

            self.setting_com_port.ConnectButton.setText("Подключиться")
            self.setting_com_port.ConnectButton.setEnabled(True)

            self.setting_com_port.flag_after_first_connect = 0
            self.setting_com_port.flag_war_msg = 1

        if(self.setting_com_port.connect_status):
            self.setting_com_port.ConnectButton.setText('Подключено')
            self.setting_com_port.ConnectButton.setEnabled(False)
            # print(setting_wind.serial_ports())

        if(setting_wind.serial_ports() != [] and not self.setting_com_port.connect_status):
            self.setting_com_port.ConnectButton.setText("Подключиться")
            self.setting_com_port.ConnectButton.setEnabled(True)
        elif(setting_wind.serial_ports() == [] and not self.setting_com_port.connect_status):
            self.setting_com_port.ConnectButton.setText("Подключиться")
            self.setting_com_port.ConnectButton.setEnabled(False)

        if(not self.setting_com_port.connect_status):
            pass

    def window_setting_open(self):
        # self.setting = setting_wind.Window2()
        # self.setting.show()
        self.setting.open()
        # self.hide()

    def window_setting_web_open(self):
        # self.setting = setting_wind.Window2()
        self.setting_web.show()
        # self.hide()

    def window_setting_tooth_open(self):
        # self.setting = setting_wind.Window2()
        self.setting_tooth.show()
        # self.hide()

    def window_setting_hammer_open(self):
        self.setting_hammer.show()

    def window_connect_com_port_open(self):
        # self.setting = setting_wind.Window2()
        self.setting_com_port.set_ports()
        self.setting_com_port.show()
        # self.hide()

    # def connect_modbus(self):
    #     self.client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
    #     # connection = self.client.connect()
    #     self.connect_status = self.client.connect()
    #     print("Connect_status", self.connect_status)

    def read_modbus(self):

        if(self.setting_com_port.connect_status):

            self.client = self.setting_com_port.client
            # write
            if(self.setting_hammer.status_but == 1):
                print("but_setting_hammer")

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(
                    self.setting_hammer.hammer_set_off_speed_min)
                registers = builder.to_registers()
                self.client.write_registers(4, registers, unit=2)

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(
                    self.setting_hammer.hammer_set_speed_on)
                registers = builder.to_registers()
                self.client.write_registers(6, registers, unit=2)

                # запись данных с регистра
                # установка времени выкл. молот, авария
                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(
                    self.setting_hammer.time_molot_off_crush)
                registers = builder.to_registers()
                self.client.write_registers(102, registers, unit=2)

                self.setting_hammer.status_but = 0

            # self.setting_tooth
            if(self.setting_tooth.status_but == 1):
                print("but_setting_tooth")
                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(
                    self.setting_tooth.val_set_off_speed_min_1)
                registers = builder.to_registers()
                self.client.write_registers(36, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_2)
                # registers = builder.to_registers()
                # self.client.write_registers(38, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_3)
                # registers = builder.to_registers()
                # self.client.write_registers(40, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_4)
                # registers = builder.to_registers()
                # self.client.write_registers(42, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_5)
                # registers = builder.to_registers()
                # self.client.write_registers(44, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_off_speed_min_6)
                # registers = builder.to_registers()
                # self.client.write_registers(46, registers, unit=2)
                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting_tooth.val_set_on_time_1)
                registers = builder.to_registers()
                self.client.write_registers(48, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_on_time_2)
                # registers = builder.to_registers()
                # self.client.write_registers(50, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_on_time_3)
                # registers = builder.to_registers()
                # self.client.write_registers(52, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_on_time_4)
                # registers = builder.to_registers()
                # self.client.write_registers(54, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_on_time_5)
                # registers = builder.to_registers()
                # self.client.write_registers(56, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_on_time_6)
                # registers = builder.to_registers()
                # self.client.write_registers(58, registers, unit=2)
                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(
                    self.setting_tooth.val_set_time_actuation_1)
                registers = builder.to_registers()
                self.client.write_registers(60, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_2)
                # registers = builder.to_registers()
                # self.client.write_registers(62, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_3)
                # registers = builder.to_registers()
                # self.client.write_registers(64, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_4)
                # registers = builder.to_registers()
                # self.client.write_registers(66, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_5)
                # registers = builder.to_registers()
                # self.client.write_registers(68, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_actuation_6)
                # registers = builder.to_registers()
                # self.client.write_registers(70, registers, unit=2)
                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(
                    self.setting_tooth.val_set_time_pulse_off_1)
                registers = builder.to_registers()
                self.client.write_registers(72, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_2)
                # registers = builder.to_registers()
                # self.client.write_registers(74, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_3)
                # registers = builder.to_registers()
                # self.client.write_registers(76, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_4)
                # registers = builder.to_registers()
                # self.client.write_registers(78, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_5)
                # registers = builder.to_registers()
                # self.client.write_registers(80, registers, unit=2)

                # builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
                # builder.add_32bit_float(self.setting_tooth.val_set_time_pulse_off_6)
                # registers = builder.to_registers()
                # self.client.write_registers(82, registers, unit=2)

                self.setting_tooth.status_but = 0

            if(self.setting.status_but == 1):
                print("but_setting")
                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_on_conveyor_1)
                registers = builder.to_registers()
                self.client.write_registers(84, registers, unit=2)

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_off_conveyor_1)
                registers = builder.to_registers()
                self.client.write_registers(86, registers, unit=2)

#
                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_on_conveyor_2)
                registers = builder.to_registers()
                self.client.write_registers(98, registers, unit=2)

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_off_conveyor_2)
                registers = builder.to_registers()
                self.client.write_registers(100, registers, unit=2)
#

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_off_hammer)
                registers = builder.to_registers()
                self.client.write_registers(88, registers, unit=2)

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.pre_launch)
                registers = builder.to_registers()
                self.client.write_registers(90, registers, unit=2)

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_direct_rotation)
                registers = builder.to_registers()
                self.client.write_registers(92, registers, unit=2)

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.time_reverse_rotation)
                registers = builder.to_registers()
                self.client.write_registers(94, registers, unit=2)

                builder = BinaryPayloadBuilder(
                    byteorder=Endian.Big, wordorder=Endian.Little)
                builder.add_32bit_float(self.setting.pause_to_stop)
                registers = builder.to_registers()
                self.client.write_registers(96, registers, unit=2)

                self.setting.status_but = 0

            s_time = time.time()

            # чтение 2х 8-bits  (reg №0)
            try:
                result = self.client.read_input_registers(0, 1,  unit=2)
                reg_0 = self.decode_2_8bit(result)
                reg_0 = copy.deepcopy(list(reversed(reg_0)))
                # print(reg_0)
                self.conveyor_on_off_1 = reg_0[0]
                self.conveyor_current_relay_1 = reg_0[1]
                self.conveyor_on_off_2 = reg_0[2]
                self.conveyor_current_relay_2 = reg_0[3]

                self.hammer_on_off = reg_0[8]
                self.hammer_crash_overload = reg_0[9]
                self.hammer_conveyor_off = reg_0[10]
                # reg_0
                # обновляем статус контроллера в 0
                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers1")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg №2-3)
            try:
                result = self.client.read_holding_registers(2, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_2_3 = decoder.decode_32bit_float()
                # print(reg_2_3)
                self.hammer_speed = reg_2_3

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers2")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_4_5)
            try:
                result = self.client.read_holding_registers(4, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_4_5 = decoder.decode_32bit_float()
                # print(reg_4_5)
                self.hammer_set_off_speed_min = reg_4_5

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers3")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_6_7)
            try:
                result = self.client.read_holding_registers(6, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_6_7 = decoder.decode_32bit_float()
                # print(reg_6_7)
                self.hammer_set_speed_on = reg_6_7

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers4")
                self.status_connect_controller = self.status_connect_controller + 1

            # время послеаварийного выключения
            # чтение float (reg_102_103)

            try:
                result = self.client.read_holding_registers(102, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_102_103 = decoder.decode_32bit_float()
                # print(reg_102_103)
                self.time_molot_off_crush = reg_102_103

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers5")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение 2х 8-bits  (reg_8)
            # вращение прямое; обратное
            try:
                result = self.client.read_input_registers(8, 1,  unit=2)
                reg_8 = self.decode_2_8bit(result)
                reg_8 = copy.deepcopy(list(reversed(reg_8)))
                # print(reg_8)
                # вращения прямое
                self.val_direction_1_p = reg_8[0]
                self.val_direction_2_p = reg_8[1]
                self.val_direction_3_p = reg_8[2]
                self.val_direction_4_p = reg_8[3]
                self.val_direction_5_p = reg_8[4]
                self.val_direction_6_p = reg_8[5]

                # вращения обратное
                self.val_direction_1_o = reg_8[8]
                self.val_direction_2_o = reg_8[9]
                self.val_direction_3_o = reg_8[10]
                self.val_direction_4_o = reg_8[11]
                self.val_direction_5_o = reg_8[12]
                self.val_direction_6_o = reg_8[13]

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers6")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение 2х 8-bits  (reg_9)
            # рабатывание токового реле, датчику скорости
            try:
                result = self.client.read_input_registers(9, 1,  unit=2)
                reg_9 = self.decode_2_8bit(result)
                reg_9 = copy.deepcopy(list(reversed(reg_9)))
                # print(reg_9)
                # срабатывание токового реле
                self.val_current_relay_1 = reg_9[0]
                self.val_current_relay_2 = reg_9[1]
                self.val_current_relay_3 = reg_9[2]
                self.val_current_relay_4 = reg_9[3]
                self.val_current_relay_5 = reg_9[4]
                self.val_current_relay_6 = reg_9[5]

                # срабатывание по датчику скорости
                self.val_sensor_speed_1 = reg_9[8]
                self.val_sensor_speed_2 = reg_9[9]
                self.val_sensor_speed_3 = reg_9[10]
                self.val_sensor_speed_4 = reg_9[11]
                self.val_sensor_speed_5 = reg_9[12]
                self.val_sensor_speed_6 = reg_9[13]

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers7")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение 2х 8-bits  (reg_10)
            # перегрев электродвигателя; зажатие вала
            try:
                result = self.client.read_input_registers(10, 1,  unit=2)
                reg_10 = self.decode_2_8bit(result)
                reg_10 = copy.deepcopy(list(reversed(reg_10)))
                # print(reg_10)
                # перегрев электродвигателя
                self.overheating_motor_1 = reg_10[0]
                self.overheating_motor_2 = reg_10[1]
                self.overheating_motor_3 = reg_10[2]
                self.overheating_motor_4 = reg_10[3]
                self.overheating_motor_5 = reg_10[4]
                self.overheating_motor_6 = reg_10[5]

                # зажатие вкл (вала)
                self.val_clamping_on_1 = reg_10[8]
                self.val_clamping_on_2 = reg_10[9]
                self.val_clamping_on_3 = reg_10[10]
                self.val_clamping_on_4 = reg_10[11]
                self.val_clamping_on_5 = reg_10[12]
                self.val_clamping_on_6 = reg_10[13]

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers8")
                self.status_connect_controller = self.status_connect_controller + 1

            # скорость вала
            # чтение float (reg_12_13)
            try:
                result = self.client.read_holding_registers(12, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_12_13 = decoder.decode_32bit_float()
                # print(reg_12_13)
                self.val_speed_1 = reg_12_13

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers9")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_14_15)
            try:
                result = self.client.read_holding_registers(14, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_14_15 = decoder.decode_32bit_float()
                # print(reg_14_15)
                self.val_speed_2 = reg_14_15

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_16_17)
            try:
                result = self.client.read_holding_registers(16, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_16_17 = decoder.decode_32bit_float()
                # print(reg_16_17)
                self.val_speed_3 = reg_16_17

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_18_19)
            try:
                result = self.client.read_holding_registers(18, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_18_19 = decoder.decode_32bit_float()
                # print(reg_18_19)
                self.val_speed_4 = reg_18_19

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_20_21)
            try:
                result = self.client.read_holding_registers(20, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_20_21 = decoder.decode_32bit_float()
                # print(reg_20_21)
                self.val_speed_5 = reg_20_21

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_22_23)
            try:
                result = self.client.read_holding_registers(22, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_22_23 = decoder.decode_32bit_float()
                # print(reg_22_23)
                self.val_speed_6 = reg_22_23

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # количество поданых реверсов
            # чтение float (reg_24_25)
            try:
                result = self.client.read_holding_registers(24, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_24_25 = decoder.decode_32bit_float()
                # print(reg_24_25)
                self.val_quantity_revers_1 = reg_24_25

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_26_27)
            try:
                result = self.client.read_holding_registers(26, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_26_27 = decoder.decode_32bit_float()
                # print(reg_26_27)
                self.val_quantity_revers_2 = reg_26_27

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_28_29)
            try:
                result = self.client.read_holding_registers(28, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_28_29 = decoder.decode_32bit_float()
                # print(reg_28_29)
                self.val_quantity_revers_3 = reg_28_29

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_30_31)
            try:
                result = self.client.read_holding_registers(30, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_30_31 = decoder.decode_32bit_float()
                # print(reg_30_31)
                self.val_quantity_revers_4 = reg_30_31

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_32_33)
            try:
                result = self.client.read_holding_registers(32, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_32_33 = decoder.decode_32bit_float()
                # print(reg_32_33)
                self.val_quantity_revers_5 = reg_32_33

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_34_35)
            try:
                result = self.client.read_holding_registers(34, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_34_35 = decoder.decode_32bit_float()
                # print(reg_34_35)
                self.val_quantity_revers_6 = reg_34_35

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # установка отключения по скорости min
            # чтение float (reg_36_37)
            try:
                result = self.client.read_holding_registers(36, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_36_37 = decoder.decode_32bit_float()
                # print(reg_36_37)
                self.val_set_off_speed_min_1 = reg_36_37

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_38_39)
            try:
                result = self.client.read_holding_registers(38, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_38_39 = decoder.decode_32bit_float()
                # print(reg_38_39)
                self.val_set_off_speed_min_2 = reg_38_39

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_40_41)
            try:
                result = self.client.read_holding_registers(40, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_40_41 = decoder.decode_32bit_float()
                # print(reg_40_41)
                self.val_set_off_speed_min_3 = reg_40_41

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_42_43)
            try:
                result = self.client.read_holding_registers(42, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_42_43 = decoder.decode_32bit_float()
                # print(reg_42_43)
                self.val_set_off_speed_min_4 = reg_42_43

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_44_45)
            try:
                result = self.client.read_holding_registers(44, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_44_45 = decoder.decode_32bit_float()
                # print(reg_44_45)
                self.val_set_off_speed_min_5 = reg_44_45

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_46_47)
            try:
                result = self.client.read_holding_registers(46, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_46_47 = decoder.decode_32bit_float()
                # print(reg_46_47)
                self.val_set_off_speed_min_6 = reg_46_47

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # установка времени включенияя
            # чтение float (reg_48_49)
            try:
                result = self.client.read_holding_registers(48, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_48_49 = decoder.decode_32bit_float()
                # print(reg_48_49)
                self.val_set_on_time_1 = reg_48_49

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_50_51)
            try:
                result = self.client.read_holding_registers(50, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_50_51 = decoder.decode_32bit_float()
                # print(reg_50_51)
                self.val_set_on_time_2 = reg_50_51

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_52_53)
            try:
                result = self.client.read_holding_registers(52, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_52_53 = decoder.decode_32bit_float()
                # print(reg_52_53)
                self.val_set_on_time_3 = reg_52_53

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_54_55)
            try:
                result = self.client.read_holding_registers(54, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_54_55 = decoder.decode_32bit_float()
                # print(reg_54_55)
                self.val_set_on_time_4 = reg_54_55

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_56_57)
            try:
                result = self.client.read_holding_registers(56, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_56_57 = decoder.decode_32bit_float()
                # print(reg_56_57)
                self.val_set_on_time_5 = reg_56_57

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1
            # чтение float (reg_58_59)
            try:
                result = self.client.read_holding_registers(58, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_58_59 = decoder.decode_32bit_float()
                # print(reg_58_59)
                self.val_set_on_time_6 = reg_58_59

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # установка времени фильтрации (срабатывания)
            # чтение float (reg_60_61)
            try:
                result = self.client.read_holding_registers(60, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_60_61 = decoder.decode_32bit_float()
                # print(reg_60_61)
                self.val_set_time_actuation_1 = reg_60_61

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_62_63)
            try:
                result = self.client.read_holding_registers(62, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_62_63 = decoder.decode_32bit_float()
                # print(reg_62_63)
                self.val_set_time_actuation_2 = reg_62_63

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_64_65)
            try:
                result = self.client.read_holding_registers(64, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_64_65 = decoder.decode_32bit_float()
                # print(reg_64_65)
                self.val_set_time_actuation_3 = reg_64_65

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_66_67)
            try:
                result = self.client.read_holding_registers(66, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_66_67 = decoder.decode_32bit_float()
                # print(reg_66_67)
                self.val_set_time_actuation_4 = reg_66_67

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_68_69)
            try:
                result = self.client.read_holding_registers(68, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_68_69 = decoder.decode_32bit_float()
                # print(reg_68_69)
                self.val_set_time_actuation_5 = reg_68_69

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_70_71)
            try:
                result = self.client.read_holding_registers(70, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_70_71 = decoder.decode_32bit_float()
                # print(reg_70_71)
                self.val_set_time_actuation_6 = reg_70_71

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # установка времени на отсутствие импульсов
            # чтение float (reg_72_73)
            try:
                result = self.client.read_holding_registers(72, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_72_73 = decoder.decode_32bit_float()
                # print(reg_72_73)
                self.val_set_time_pulse_off_1 = reg_72_73

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_74_75)
            try:
                result = self.client.read_holding_registers(74, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_74_75 = decoder.decode_32bit_float()
                # print(reg_74_75)
                self.val_set_time_pulse_off_2 = reg_74_75

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_76_77)
            try:
                result = self.client.read_holding_registers(76, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_76_77 = decoder.decode_32bit_float()
                # print(reg_76_77)
                self.val_set_time_pulse_off_3 = reg_76_77

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_78_79)
            try:
                result = self.client.read_holding_registers(78, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_78_79 = decoder.decode_32bit_float()
                # print(reg_78_79)
                self.val_set_time_pulse_off_4 = reg_78_79

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_80_81)
            try:
                result = self.client.read_holding_registers(80, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_80_81 = decoder.decode_32bit_float()
                # print(reg_80_81)
                self.val_set_time_pulse_off_5 = reg_80_81

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # чтение float (reg_82_83)
            try:
                result = self.client.read_holding_registers(82, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_82_83 = decoder.decode_32bit_float()
                # print(reg_82_83)
                self.val_set_time_pulse_off_6 = reg_82_83

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # настройки
            # время вкл выкидного транспортера
            # чтение float (reg_84_85)
            try:
                result = self.client.read_holding_registers(84, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_84_85 = decoder.decode_32bit_float()
                # print(reg_84_85)
                self.time_on_conveyor_1 = reg_84_85
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # время выкл выкидного транспортера
            # чтение float (reg_86_87)
            try:
                result = self.client.read_holding_registers(86, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_86_87 = decoder.decode_32bit_float()
                # print(reg_86_87)
                self.time_off_conveyor_1 = reg_86_87

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # время вкл подающего транспортера
            # чтение float (reg_98_99)
            try:
                result = self.client.read_holding_registers(98, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_98_99 = decoder.decode_32bit_float()
                # print(reg_84_85)
                self.time_on_conveyor_2 = reg_98_99

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # время выкл подающего транспортера
            # чтение float (reg_100_101)
            try:
                result = self.client.read_holding_registers(100, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_100_101 = decoder.decode_32bit_float()
                # print(reg_100_101)
                self.time_off_conveyor_2 = reg_100_101

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # время выключения молотковой дробилки
            # чтение float (reg_88_89)
            try:
                result = self.client.read_holding_registers(88, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_88_89 = decoder.decode_32bit_float()
                # print(reg_88_89)
                self.time_off_hammer = reg_88_89

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1
            # предпусковой звонок
            # чтение float (reg_90_91)
            try:
                result = self.client.read_holding_registers(90, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_90_91 = decoder.decode_32bit_float()
                # print(reg_90_91)
                self.pre_launch = reg_90_91

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # время прямого вращения
            # чтение float (reg_92_93)
            try:
                result = self.client.read_holding_registers(92, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_92_93 = decoder.decode_32bit_float()
                # print(reg_92_93)
                self.time_direct_rotation = reg_92_93

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # время обратного вражения
            # чтение float (reg_94_95)
            try:
                result = self.client.read_holding_registers(94, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_94_95 = decoder.decode_32bit_float()
                # print(reg_94_95)
                self.time_reverse_rotation = reg_94_95

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            # пауза на остановку
            # чтение float (reg_96_97)
            try:
                result = self.client.read_holding_registers(96, 2,  unit=2)
                decoder = BinaryPayloadDecoder.fromRegisters(
                    result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
                reg_96_97 = decoder.decode_32bit_float()
                # print(reg_96_97)
                self.pause_to_stop = reg_96_97

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers")
                self.status_connect_controller = self.status_connect_controller + 1

            f_time = time.time()
            d_time = f_time - s_time
            # print(d_time)


            # чтение 2х 8-bits  (reg_104)
            # режим работы
            try:
                result = self.client.read_input_registers(104, 1,  unit=2)
                reg_104 = self.decode_2_8bit(result)
                reg_104 = copy.deepcopy(list(reversed(reg_104)))
                # print(reg_104)
                # режимы работы
                self.auto_mode = reg_104[0]
                self.hand_mode = reg_104[1]
                self.none_mode = reg_104[2]

                self.status_connect_controller = 0
            except AttributeError:
                print("ModbusIOException object has no attribute registers 104")
                self.status_connect_controller = self.status_connect_controller + 1



    def decode_2_8bit(self, data):

        # print("-"*60)
        # print(data.registers)
        # print()

        line_registers = "{0:b}".format(data.registers[0])

        array_reg = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        index = len(array_reg)-1
        size_line = len(line_registers)
        index_line = size_line - 1

        while(index_line >= 0):
            array_reg[index] = int(line_registers[index_line])
            index = index - 1
            index_line = index_line - 1

        # print(array_reg)
        return array_reg


class Test_mp(QThread):
    def __init__(self, mainwindow, parent=None):
        super().__init__()
        self.mainwindow = mainwindow
        self.value = 0

    def run(self):
        # value = self.mainwindow.prograsbas.value()
        # print("hjkml,")
        while 1:
            self.value = self.value + 1
            # self.mainwindow.prograsbas.setValue(value)
            try:
                self.mainwindow.read_modbus()
                # print(self.value)
                # time.sleep(0.2)
            except:
                print("connect error")
                print(self.mainwindow.setting_com_port.connect_status)
                print(self.mainwindow.setting_com_port.client.connect())
                # QMessageBox.warning(None, 'Warning', 'Invalid password!')
                # quit()


# class VK_mp(QThread):
#     def __init__(self, mainwindow_2, parent=None):
#         super().__init__()
#         self.mainwindow = mainwindow_2
#         self.value = 0
#         self.start_time = time.time()


#     def run(self):
#         # value = self.mainwindow.prograsbas.value()
#         while 1:
#             # print("hjkml,")
#             # print(time.time())
#             # if(time.time()-self.start_time> 5):

#             # self.start_time = time.time()
#             self.value = self.value + 1
#             # self.mainwindow.prograsbas.setValue(value)
#             try:
#                 self.mainwindow.put_data_VK()
#                 time.sleep(5)
#                 # self.mainwindow.read_modbus()
#                 # print(self.value)
#                 # time.sleep(0.2)
#             except:
#                 print("connect error_vk")


class SQL_mp(QThread):
    def __init__(self, mainwindow_3, parent=None):
        super().__init__()
        self.mainwindow = mainwindow_3
        self.value = 0
        self.start_time = time.time()

    def run(self):
        # value = self.mainwindow.prograsbas.value()
        while 1:
            # print("hjkml,")
            # print(time.time())
            # if(time.time()-self.start_time> 5):

            # self.start_time = time.time()
            self.value = self.value + 1
            # self.mainwindow.prograsbas.setValue(value)
            try:
                self.mainwindow.put_data_to_SQL()
                time.sleep(5)
                # self.mainwindow.read_modbus()
                # print(self.value)
                # time.sleep(0.2)
            except:
                print("connect error_sql")


if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)

    # ss = []

    splash = QSplashScreen(QPixmap("img/гифка steelcom/png/ck_logo000"))
    splash.show()
    for i in range(148):
        if i < 10:
            name = "ck_logo00" + str(i) + ".png"
        if i >= 10 and i < 100:
            name = "ck_logo0" + str(i) + ".png"
        if i >= 100:
            name = "ck_logo" + str(i) + ".png"
        pixmap = QPixmap("img/гифка steelcom/png/" + name)

        # ss.append(pixmap)
        splash.setPixmap(pixmap)
        # time.sleep(0.01)
        time.sleep(0)

    # for i in ss:
    #
    #     splash.setPixmap(i)
    #     time.sleep(0.01)
    # pixmap = QPixmap("img/гифка steelcom/гифка.gif")
    # splash = QSplashScreen(pixmap)
    # splash.show()
    # app.processEvents()

    ex = Window_Ui()
    # menus = MenuDemo()
    # ex = Main_Window()
    sys.exit(app.exec_())
